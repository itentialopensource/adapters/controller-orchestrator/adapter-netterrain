# NetTerrain

Vendor: Graphical Networks
Homepage: https://graphicalnetworks.com/

Product: netTerrain
Product Page: https://graphicalnetworks.com/products/netterrain-logical/

## Introduction
We classify Netterrain into the Data Center and Network Services domain as Netterrain provides a unified view of IT infrastructure, making it easy to monitor, troubleshoot, and optimize resources across various environments.

## Why Integrate
The Netterrain adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Netterrain. With this adapter you have the ability to perform operations on items such as:

- Nodes
- Circuits
- Work Orders

## Additional Product Documentation
The [API documents for Netterrain]()
