
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:21PM

See merge request itentialopensource/adapters/adapter-netterrain!12

---

## 0.4.3 [09-12-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-netterrain!10

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:33PM

See merge request itentialopensource/adapters/adapter-netterrain!9

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:45PM

See merge request itentialopensource/adapters/adapter-netterrain!8

---

## 0.4.0 [07-10-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netterrain!7

---

## 0.3.4 [03-28-2024]

* Changes made at 2024.03.28_13:38PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netterrain!6

---

## 0.3.3 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netterrain!5

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_15:57PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netterrain!4

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:25AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netterrain!3

---

## 0.3.0 [12-28-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netterrain!2

---

## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netterrain!1

---

## 0.1.1 [08-30-2021]

- Initial Commit

See commit f1c7926

---
