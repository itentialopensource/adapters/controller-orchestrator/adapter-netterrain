
## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-netterrain!1

---

## 0.1.1 [08-30-2021]

- Initial Commit

See commit f1c7926

---
