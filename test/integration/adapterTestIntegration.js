/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-netterrain',
      type: 'Netterrain',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Netterrain = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Netterrain Adapter Test', () => {
  describe('Netterrain Class Tests', () => {
    const a = new Netterrain(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getWapiV1Cards - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1Cards(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesCards', 'getWapiV1Cards', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesCardsId = 555;
    const nodesCardsRESTfulCardsPutBodyParam = {};
    describe('#rESTfulCardsPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulCardsPut(nodesCardsId, nodesCardsRESTfulCardsPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesCards', 'rESTfulCardsPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCardsGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulCardsGetById(nodesCardsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.base_node);
                assert.equal('object', typeof data.response.slot_representation);
                assert.equal('object', typeof data.response.card_type);
                assert.equal(6, data.response.peak_power);
                assert.equal(8, data.response.weight);
                assert.equal('string', data.response.status);
                assert.equal('object', typeof data.response.cards);
                assert.equal('object', typeof data.response.slots);
                assert.equal('object', typeof data.response.ports);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesCards', 'rESTfulCardsGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesCardsParentTypeGroup = 'fakedata';
    const nodesCardsParentId = 555;
    describe('#rESTfulCardsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulCardsGet(nodesCardsParentTypeGroup, nodesCardsParentId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesCards', 'rESTfulCardsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1Ports - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1Ports(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesPorts', 'getWapiV1Ports', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesPortsTypeGroup = 'fakedata';
    const nodesPortsNodeId = 555;
    describe('#rESTfulPortsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulPortsGet(nodesPortsTypeGroup, nodesPortsNodeId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesPorts', 'rESTfulPortsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesPortsId = 555;
    describe('#rESTfulPortsGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulPortsGetById(nodesPortsTypeGroup, nodesPortsNodeId, nodesPortsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.parent_node);
                assert.equal('object', typeof data.response.port_representation);
                assert.equal('object', typeof data.response.node_property_values);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesPorts', 'rESTfulPortsGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linksRESTfulLinksPostBodyParam = {
      name: 'string',
      link_type: {
        href: 'string'
      },
      from_node: {
        href: 'string'
      },
      to_node: {
        href: 'string'
      },
      link_property_values: {
        href: 'string'
      },
      vis_links: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#rESTfulLinksPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulLinksPost(linksRESTfulLinksPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Links', 'rESTfulLinksPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linksRESTfulLinksPutBodyParam = {
      name: 'string',
      link_type: {
        href: 'string'
      },
      from_node: {
        href: 'string'
      },
      to_node: {
        href: 'string'
      },
      link_property_values: {
        href: 'string'
      },
      vis_links: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#rESTfulLinksPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulLinksPut(linksRESTfulLinksPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Links', 'rESTfulLinksPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1Links - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1Links(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Links', 'getWapiV1Links', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linksId = 555;
    const linksPutWapiV1LinksIdBodyParam = {
      name: 'string',
      link_type: {
        href: 'string'
      },
      from_node: {
        href: 'string'
      },
      to_node: {
        href: 'string'
      },
      link_property_values: {
        href: 'string'
      },
      vis_links: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#putWapiV1LinksId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1LinksId(linksId, linksPutWapiV1LinksIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Links', 'putWapiV1LinksId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinksGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulLinksGetById(linksId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.link_type);
                assert.equal('object', typeof data.response.from_node);
                assert.equal('object', typeof data.response.to_node);
                assert.equal('object', typeof data.response.link_property_values);
                assert.equal('object', typeof data.response.vis_links);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Links', 'rESTfulLinksGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linksTypeGroup = 'fakedata';
    const linksNodeId = 555;
    describe('#rESTfulLinksGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulLinksGet(linksTypeGroup, linksNodeId, linksId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Links', 'rESTfulLinksGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesSlotsTypeGroup = 'fakedata';
    const nodesSlotsNodeId = 555;
    const nodesSlotsId = 555;
    const nodesSlotsRESTfulSlotsPostBodyParam = {
      parent: {
        href: 'string'
      },
      name: 'string',
      base_node: {
        href: 'string'
      },
      slot_representation: {
        href: 'string'
      },
      card_type: {
        href: 'string'
      },
      peak_power: 6,
      weight: 5,
      status: 'string',
      cards: {
        href: 'string'
      },
      slots: {
        href: 'string'
      },
      ports: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#rESTfulSlotsPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulSlotsPost(nodesSlotsTypeGroup, nodesSlotsNodeId, nodesSlotsId, nodesSlotsRESTfulSlotsPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesSlots', 'rESTfulSlotsPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1Slots - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1Slots(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesSlots', 'getWapiV1Slots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulSlotsGet(nodesSlotsTypeGroup, nodesSlotsNodeId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesSlots', 'rESTfulSlotsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotsGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulSlotsGetById(nodesSlotsTypeGroup, nodesSlotsNodeId, nodesSlotsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.base_node);
                assert.equal('object', typeof data.response.slot_representation);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesSlots', 'rESTfulSlotsGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesPortRepresentationsPostWapiV1PortRepresentationsBodyParam = {
      parent_type: {
        href: 'string'
      },
      name: 'string',
      x: 2,
      y: 3,
      width: 5,
      height: 3,
      angle: 7,
      anchor_point_x: 6,
      anchor_point_y: 5,
      ref_x: 9,
      ref_y: 7,
      ref_width: 1,
      ref_height: 3,
      ref_angle: 7,
      ref_anchor_point_x: 2,
      ref_anchor_point_y: 7,
      bendpoint_path: 'string',
      is_hidden: false,
      port_fields: {
        href: 'string'
      },
      node_type: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#postWapiV1PortRepresentations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postWapiV1PortRepresentations(nodeTypesPortRepresentationsPostWapiV1PortRepresentationsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPortRepresentations', 'postWapiV1PortRepresentations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesPortRepresentationsTypeGroup = 'fakedata';
    const nodeTypesPortRepresentationsTypeId = 555;
    const nodeTypesPortRepresentationsRESTfulPortRepresentationsPostBodyParam = {
      parent_type: {
        href: 'string'
      },
      name: 'string',
      x: 6,
      y: 9,
      width: 2,
      height: 5,
      angle: 8,
      anchor_point_x: 7,
      anchor_point_y: 3,
      ref_x: 8,
      ref_y: 2,
      ref_width: 7,
      ref_height: 1,
      ref_angle: 5,
      ref_anchor_point_x: 4,
      ref_anchor_point_y: 6,
      bendpoint_path: 'string',
      is_hidden: true,
      port_fields: {
        href: 'string'
      },
      node_type: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#rESTfulPortRepresentationsPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulPortRepresentationsPost(nodeTypesPortRepresentationsTypeGroup, nodeTypesPortRepresentationsTypeId, nodeTypesPortRepresentationsRESTfulPortRepresentationsPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPortRepresentations', 'rESTfulPortRepresentationsPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesPortRepresentationsPutWapiV1PortRepresentationsBodyParam = {
      parent_type: {
        href: 'string'
      },
      name: 'string',
      x: 7,
      y: 10,
      width: 8,
      height: 8,
      angle: 4,
      anchor_point_x: 3,
      anchor_point_y: 6,
      ref_x: 5,
      ref_y: 10,
      ref_width: 10,
      ref_height: 4,
      ref_angle: 1,
      ref_anchor_point_x: 9,
      ref_anchor_point_y: 8,
      bendpoint_path: 'string',
      is_hidden: true,
      port_fields: {
        href: 'string'
      },
      node_type: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#putWapiV1PortRepresentations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1PortRepresentations(nodeTypesPortRepresentationsPutWapiV1PortRepresentationsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPortRepresentations', 'putWapiV1PortRepresentations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1PortRepresentations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1PortRepresentations(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPortRepresentations', 'getWapiV1PortRepresentations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesPortRepresentationsId = 555;
    describe('#getWapiV1PortRepresentationsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1PortRepresentationsId(nodeTypesPortRepresentationsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPortRepresentations', 'getWapiV1PortRepresentationsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulPortRepresentationsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulPortRepresentationsGet(nodeTypesPortRepresentationsTypeGroup, nodeTypesPortRepresentationsTypeId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPortRepresentations', 'rESTfulPortRepresentationsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesPortRepresentationsRESTfulPortRepresentationsPutBodyParam = {
      parent_type: {
        href: 'string'
      },
      name: 'string',
      x: 7,
      y: 9,
      width: 1,
      height: 3,
      angle: 4,
      anchor_point_x: 5,
      anchor_point_y: 10,
      ref_x: 7,
      ref_y: 4,
      ref_width: 10,
      ref_height: 7,
      ref_angle: 3,
      ref_anchor_point_x: 8,
      ref_anchor_point_y: 1,
      bendpoint_path: 'string',
      is_hidden: true,
      port_fields: {
        href: 'string'
      },
      node_type: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#rESTfulPortRepresentationsPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulPortRepresentationsPut(nodeTypesPortRepresentationsTypeGroup, nodeTypesPortRepresentationsTypeId, nodeTypesPortRepresentationsId, nodeTypesPortRepresentationsRESTfulPortRepresentationsPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPortRepresentations', 'rESTfulPortRepresentationsPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulPortRepresentationsGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulPortRepresentationsGetById(nodeTypesPortRepresentationsTypeGroup, nodeTypesPortRepresentationsTypeId, nodeTypesPortRepresentationsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_type);
                assert.equal('string', data.response.name);
                assert.equal(1, data.response.x);
                assert.equal(4, data.response.y);
                assert.equal(7, data.response.width);
                assert.equal(9, data.response.height);
                assert.equal(4, data.response.angle);
                assert.equal(7, data.response.anchor_point_x);
                assert.equal(1, data.response.anchor_point_y);
                assert.equal(6, data.response.ref_x);
                assert.equal(6, data.response.ref_y);
                assert.equal(9, data.response.ref_width);
                assert.equal(4, data.response.ref_height);
                assert.equal(5, data.response.ref_angle);
                assert.equal(5, data.response.ref_anchor_point_x);
                assert.equal(1, data.response.ref_anchor_point_y);
                assert.equal('string', data.response.bendpoint_path);
                assert.equal(false, data.response.is_hidden);
                assert.equal('object', typeof data.response.port_fields);
                assert.equal('object', typeof data.response.node_type);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPortRepresentations', 'rESTfulPortRepresentationsGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1PortFields - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1PortFields(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPortRepresentationsPortFields', 'getWapiV1PortFields', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesPortRepresentationsPortFieldsId = 555;
    describe('#getWapiV1PortFieldsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1PortFieldsId(nodeTypesPortRepresentationsPortFieldsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPortRepresentationsPortFields', 'getWapiV1PortFieldsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesPortRepresentationsPortFieldsPortRepresentationId = 555;
    const nodeTypesPortRepresentationsPortFieldsTypeGroup = 'fakedata';
    const nodeTypesPortRepresentationsPortFieldsTypeId = 555;
    describe('#rESTfulPortFieldsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulPortFieldsGet(nodeTypesPortRepresentationsPortFieldsPortRepresentationId, nodeTypesPortRepresentationsPortFieldsTypeGroup, nodeTypesPortRepresentationsPortFieldsTypeId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPortRepresentationsPortFields', 'rESTfulPortFieldsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulPortFieldsGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulPortFieldsGetById(nodeTypesPortRepresentationsPortFieldsTypeGroup, nodeTypesPortRepresentationsPortFieldsTypeId, nodeTypesPortRepresentationsPortFieldsPortRepresentationId, nodeTypesPortRepresentationsPortFieldsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_port_representation);
                assert.equal(true, data.response.is_reference_port);
                assert.equal('object', typeof data.response.node_type_property);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.value);
                assert.equal(0, data.response.text_justification);
                assert.equal(2, data.response.text_align);
                assert.equal(0, data.response.font_family);
                assert.equal(8, data.response.font_size);
                assert.equal('string', data.response.font_color);
                assert.equal('string', data.response.fill_color);
                assert.equal(8, data.response.offset_x);
                assert.equal(6, data.response.offset_y);
                assert.equal(7, data.response.angle);
                assert.equal(false, data.response.is_bold);
                assert.equal(false, data.response.is_italic);
                assert.equal(false, data.response.is_underlined);
                assert.equal(true, data.response.is_displayed);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPortRepresentationsPortFields', 'rESTfulPortFieldsGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesSlotRepresentationsPostWapiV1SlotRepresentationsBodyParam = {
      parent_type: {
        href: 'string'
      },
      name: 'string',
      x: 6,
      y: 7,
      width: 2,
      height: 8,
      angle: 1,
      slot_mapping: {
        href: 'string'
      },
      slot_fields: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#postWapiV1SlotRepresentations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postWapiV1SlotRepresentations(nodeTypesSlotRepresentationsPostWapiV1SlotRepresentationsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentations', 'postWapiV1SlotRepresentations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesSlotRepresentationsTypeGroup = 'fakedata';
    const nodeTypesSlotRepresentationsTypeId = 555;
    const nodeTypesSlotRepresentationsRESTfulSlotRepresentationsPostBodyParam = {
      parent_type: {
        href: 'string'
      },
      name: 'string',
      x: 3,
      y: 8,
      width: 4,
      height: 6,
      angle: 7,
      slot_mapping: {
        href: 'string'
      },
      slot_fields: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#rESTfulSlotRepresentationsPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulSlotRepresentationsPost(nodeTypesSlotRepresentationsTypeGroup, nodeTypesSlotRepresentationsTypeId, nodeTypesSlotRepresentationsRESTfulSlotRepresentationsPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentations', 'rESTfulSlotRepresentationsPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesSlotRepresentationsPutWapiV1SlotRepresentationsBodyParam = {
      parent_type: {
        href: 'string'
      },
      name: 'string',
      x: 6,
      y: 3,
      width: 6,
      height: 6,
      angle: 4,
      slot_mapping: {
        href: 'string'
      },
      slot_fields: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#putWapiV1SlotRepresentations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1SlotRepresentations(nodeTypesSlotRepresentationsPutWapiV1SlotRepresentationsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentations', 'putWapiV1SlotRepresentations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1SlotRepresentations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1SlotRepresentations(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentations', 'getWapiV1SlotRepresentations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesSlotRepresentationsId = 555;
    describe('#getWapiV1SlotRepresentationsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1SlotRepresentationsId(nodeTypesSlotRepresentationsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentations', 'getWapiV1SlotRepresentationsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotRepresentationsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulSlotRepresentationsGet(nodeTypesSlotRepresentationsTypeGroup, nodeTypesSlotRepresentationsTypeId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentations', 'rESTfulSlotRepresentationsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesSlotRepresentationsRESTfulSlotRepresentationsPutBodyParam = {
      parent_type: {
        href: 'string'
      },
      name: 'string',
      x: 8,
      y: 8,
      width: 7,
      height: 9,
      angle: 5,
      slot_mapping: {
        href: 'string'
      },
      slot_fields: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#rESTfulSlotRepresentationsPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulSlotRepresentationsPut(nodeTypesSlotRepresentationsTypeGroup, nodeTypesSlotRepresentationsTypeId, nodeTypesSlotRepresentationsId, nodeTypesSlotRepresentationsRESTfulSlotRepresentationsPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentations', 'rESTfulSlotRepresentationsPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotRepresentationsGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulSlotRepresentationsGetById(nodeTypesSlotRepresentationsTypeGroup, nodeTypesSlotRepresentationsTypeId, nodeTypesSlotRepresentationsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_type);
                assert.equal('string', data.response.name);
                assert.equal(6, data.response.x);
                assert.equal(2, data.response.y);
                assert.equal(8, data.response.width);
                assert.equal(4, data.response.height);
                assert.equal(1, data.response.angle);
                assert.equal('object', typeof data.response.slot_mapping);
                assert.equal('object', typeof data.response.slot_fields);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentations', 'rESTfulSlotRepresentationsGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1SlotFields - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1SlotFields(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentationsSlotFields', 'getWapiV1SlotFields', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesSlotRepresentationsSlotFieldsId = 555;
    describe('#getWapiV1SlotFieldsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1SlotFieldsId(nodeTypesSlotRepresentationsSlotFieldsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentationsSlotFields', 'getWapiV1SlotFieldsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesSlotRepresentationsSlotFieldsSlotRepresentationId = 555;
    const nodeTypesSlotRepresentationsSlotFieldsTypeGroup = 'fakedata';
    const nodeTypesSlotRepresentationsSlotFieldsTypeId = 555;
    describe('#rESTfulSlotFieldsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulSlotFieldsGet(nodeTypesSlotRepresentationsSlotFieldsSlotRepresentationId, nodeTypesSlotRepresentationsSlotFieldsTypeGroup, nodeTypesSlotRepresentationsSlotFieldsTypeId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentationsSlotFields', 'rESTfulSlotFieldsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotFieldsGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulSlotFieldsGetById(nodeTypesSlotRepresentationsSlotFieldsTypeGroup, nodeTypesSlotRepresentationsSlotFieldsTypeId, nodeTypesSlotRepresentationsSlotFieldsSlotRepresentationId, nodeTypesSlotRepresentationsSlotFieldsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_slot_representation);
                assert.equal('object', typeof data.response.node_type_property);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.value);
                assert.equal(1, data.response.text_justification);
                assert.equal(2, data.response.text_align);
                assert.equal(8, data.response.font_family);
                assert.equal(5, data.response.font_size);
                assert.equal('string', data.response.font_color);
                assert.equal('string', data.response.fill_color);
                assert.equal(9, data.response.offset_x);
                assert.equal(6, data.response.offset_y);
                assert.equal(6, data.response.angle);
                assert.equal(false, data.response.is_bold);
                assert.equal(false, data.response.is_italic);
                assert.equal(true, data.response.is_underlined);
                assert.equal(true, data.response.is_displayed);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentationsSlotFields', 'rESTfulSlotFieldsGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesSlotRepresentationsSlotMappingsPostWapiV1SlotMappingBodyParam = {
      slot_representation: {
        href: 'string'
      },
      card_type: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#postWapiV1SlotMapping - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postWapiV1SlotMapping(nodeTypesSlotRepresentationsSlotMappingsPostWapiV1SlotMappingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentationsSlotMappings', 'postWapiV1SlotMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesSlotRepresentationsSlotMappingsSlotRepresentationId = 555;
    const nodeTypesSlotRepresentationsSlotMappingsTypeGroup = 'fakedata';
    const nodeTypesSlotRepresentationsSlotMappingsTypeId = 555;
    const nodeTypesSlotRepresentationsSlotMappingsRESTfulSlotMappingPostBodyParam = {
      slot_representation: {
        href: 'string'
      },
      card_type: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#rESTfulSlotMappingPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulSlotMappingPost(nodeTypesSlotRepresentationsSlotMappingsTypeGroup, nodeTypesSlotRepresentationsSlotMappingsTypeId, nodeTypesSlotRepresentationsSlotMappingsSlotRepresentationId, nodeTypesSlotRepresentationsSlotMappingsRESTfulSlotMappingPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentationsSlotMappings', 'rESTfulSlotMappingPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1SlotMapping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1SlotMapping(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentationsSlotMappings', 'getWapiV1SlotMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesSlotRepresentationsSlotMappingsId = 555;
    describe('#getWapiV1SlotMappingId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1SlotMappingId(nodeTypesSlotRepresentationsSlotMappingsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentationsSlotMappings', 'getWapiV1SlotMappingId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotMappingGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulSlotMappingGet(nodeTypesSlotRepresentationsSlotMappingsSlotRepresentationId, nodeTypesSlotRepresentationsSlotMappingsTypeGroup, nodeTypesSlotRepresentationsSlotMappingsTypeId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentationsSlotMappings', 'rESTfulSlotMappingGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotMappingGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulSlotMappingGetById(nodeTypesSlotRepresentationsSlotMappingsTypeGroup, nodeTypesSlotRepresentationsSlotMappingsTypeId, nodeTypesSlotRepresentationsSlotMappingsSlotRepresentationId, nodeTypesSlotRepresentationsSlotMappingsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.slot_representation);
                assert.equal('object', typeof data.response.card_type);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentationsSlotMappings', 'rESTfulSlotMappingGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesCardTypesRESTfulCardTypesPostBodyParam = {};
    describe('#rESTfulCardTypesPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulCardTypesPost(nodeTypesCardTypesRESTfulCardTypesPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesCardTypes', 'rESTfulCardTypesPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesCardTypesRESTfulCardTypesPutBodyParam = {};
    describe('#rESTfulCardTypesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulCardTypesPut(nodeTypesCardTypesRESTfulCardTypesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesCardTypes', 'rESTfulCardTypesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCardTypesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulCardTypesGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesCardTypes', 'rESTfulCardTypesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesCardTypesId = 555;
    const nodeTypesCardTypesPutWapiV1CardTypesIdBodyParam = {};
    describe('#putWapiV1CardTypesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1CardTypesId(nodeTypesCardTypesId, nodeTypesCardTypesPutWapiV1CardTypesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesCardTypes', 'putWapiV1CardTypesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCardTypesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulCardTypesGetById(nodeTypesCardTypesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.base_node_type);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.background);
                assert.equal(4, data.response.peak_power);
                assert.equal(3, data.response.weight);
                assert.equal(6, data.response.physical_height);
                assert.equal(4, data.response.physical_width);
                assert.equal('object', typeof data.response.vendor);
                assert.equal('object', typeof data.response.port_representations);
                assert.equal('object', typeof data.response.slot_representations);
                assert.equal('object', typeof data.response.mapped_to);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesCardTypes', 'rESTfulCardTypesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitPropertiesRESTfulCircuitPropertiesPostBodyParam = {
      name: 'string',
      is_system: false,
      show_in_acra: false,
      href: 'string'
    };
    describe('#rESTfulCircuitPropertiesPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulCircuitPropertiesPost(circuitPropertiesRESTfulCircuitPropertiesPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CircuitProperties', 'rESTfulCircuitPropertiesPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitPropertiesRESTfulCircuitPropertiesPutBodyParam = {
      name: 'string',
      is_system: false,
      show_in_acra: true,
      href: 'string'
    };
    describe('#rESTfulCircuitPropertiesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulCircuitPropertiesPut(circuitPropertiesRESTfulCircuitPropertiesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CircuitProperties', 'rESTfulCircuitPropertiesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCircuitPropertiesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulCircuitPropertiesGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CircuitProperties', 'rESTfulCircuitPropertiesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitPropertiesId = 555;
    describe('#rESTfulCircuitPropertiesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulCircuitPropertiesGetById(circuitPropertiesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal(true, data.response.is_system);
                assert.equal(false, data.response.show_in_acra);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CircuitProperties', 'rESTfulCircuitPropertiesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitsPropertyValuesCircuitId = 555;
    describe('#rESTfulCircuitPropertyValuesInitAbsentValues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulCircuitPropertyValuesInitAbsentValues(circuitsPropertyValuesCircuitId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CircuitsPropertyValues', 'rESTfulCircuitPropertyValuesInitAbsentValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCircuitPropertyValuesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulCircuitPropertyValuesGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CircuitsPropertyValues', 'rESTfulCircuitPropertyValuesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitsPropertyValuesRESTfulCircuitPropertyValuesPutBodyParam = {
      name: 'string',
      parent_circuit: {
        href: 'string'
      },
      circuit_property: {
        href: 'string'
      },
      value: 'string',
      href: 'string'
    };
    describe('#rESTfulCircuitPropertyValuesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulCircuitPropertyValuesPut(circuitsPropertyValuesCircuitId, circuitsPropertyValuesRESTfulCircuitPropertyValuesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CircuitsPropertyValues', 'rESTfulCircuitPropertyValuesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1CircuitsCircuitIdCircuitPropertyValues - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1CircuitsCircuitIdCircuitPropertyValues(circuitsPropertyValuesCircuitId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CircuitsPropertyValues', 'getWapiV1CircuitsCircuitIdCircuitPropertyValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitsPropertyValuesId = 555;
    describe('#rESTfulCircuitPropertyValuesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulCircuitPropertyValuesGetById(circuitsPropertyValuesCircuitId, circuitsPropertyValuesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.parent_circuit);
                assert.equal('object', typeof data.response.circuit_property);
                assert.equal('string', data.response.value);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CircuitsPropertyValues', 'rESTfulCircuitPropertyValuesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitsRESTfulCircuitsPostBodyParam = {};
    describe('#rESTfulCircuitsPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulCircuitsPost(circuitsRESTfulCircuitsPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'rESTfulCircuitsPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCircuitsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulCircuitsGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'rESTfulCircuitsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitsId = 555;
    describe('#rESTfulCircuitsGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulCircuitsGetById(circuitsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.from_node);
                assert.equal('object', typeof data.response.to_node);
                assert.equal('object', typeof data.response.circuit_property_values);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'rESTfulCircuitsGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesDeviceTypesRESTfulDeviceTypesPostBodyParam = {};
    describe('#rESTfulDeviceTypesPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulDeviceTypesPost(nodeTypesDeviceTypesRESTfulDeviceTypesPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesDeviceTypes', 'rESTfulDeviceTypesPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesDeviceTypesRESTfulDeviceTypesPutBodyParam = {};
    describe('#rESTfulDeviceTypesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulDeviceTypesPut(nodeTypesDeviceTypesRESTfulDeviceTypesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesDeviceTypes', 'rESTfulDeviceTypesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDeviceTypesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulDeviceTypesGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesDeviceTypes', 'rESTfulDeviceTypesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesDeviceTypesId = 555;
    const nodeTypesDeviceTypesPutWapiV1DeviceTypesIdBodyParam = {};
    describe('#putWapiV1DeviceTypesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1DeviceTypesId(nodeTypesDeviceTypesId, nodeTypesDeviceTypesPutWapiV1DeviceTypesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesDeviceTypes', 'putWapiV1DeviceTypesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDeviceTypesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulDeviceTypesGetById(nodeTypesDeviceTypesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.system_object_id);
                assert.equal(10, data.response.in_stock);
                assert.equal('object', typeof data.response.base_node_type);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.background);
                assert.equal(6, data.response.peak_power);
                assert.equal(5, data.response.weight);
                assert.equal(8, data.response.physical_height);
                assert.equal(1, data.response.physical_width);
                assert.equal('object', typeof data.response.vendor);
                assert.equal('object', typeof data.response.port_representations);
                assert.equal('object', typeof data.response.slot_representations);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesDeviceTypes', 'rESTfulDeviceTypesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesDevicesRESTfulDevicesPostBodyParam = {};
    describe('#rESTfulDevicesPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulDevicesPost(nodesDevicesRESTfulDevicesPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesDevices', 'rESTfulDevicesPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDevicesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulDevicesGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesDevices', 'rESTfulDevicesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesDevicesId = 555;
    const nodesDevicesRESTfulDevicesPutBodyParam = {};
    describe('#rESTfulDevicesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulDevicesPut(nodesDevicesId, nodesDevicesRESTfulDevicesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesDevices', 'rESTfulDevicesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDevicesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulDevicesGetById(nodesDevicesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.base_node);
                assert.equal('object', typeof data.response.parent_rack_container);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('string', data.response.name);
                assert.equal(10, data.response.peak_power);
                assert.equal(1, data.response.weight);
                assert.equal('string', data.response.status);
                assert.equal(9, data.response.container_position);
                assert.equal(3, data.response.rack_units_occupied);
                assert.equal('object', typeof data.response.cards);
                assert.equal('object', typeof data.response.slots);
                assert.equal('object', typeof data.response.ports);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesDevices', 'rESTfulDevicesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesDevicesRackId = 555;
    const nodesDevicesContainerId = 555;
    const nodesDevicesRESTfulDevicesMountDeviceBodyParam = {
      base_node: {
        href: 'string'
      },
      parent_rack_container: {
        href: 'string'
      },
      device_type: {
        href: 'string'
      },
      name: 'string',
      peak_power: 3,
      weight: 8,
      status: 'string',
      container_position: 6,
      rack_units_occupied: 4,
      cards: {
        href: 'string'
      },
      slots: {
        href: 'string'
      },
      ports: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#rESTfulDevicesMountDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulDevicesMountDevice(nodesDevicesRackId, nodesDevicesContainerId, nodesDevicesRESTfulDevicesMountDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesDevices', 'rESTfulDevicesMountDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1RacksRackIdContainersContainerIdMountedDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1RacksRackIdContainersContainerIdMountedDevices(nodesDevicesRackId, nodesDevicesContainerId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesDevices', 'getWapiV1RacksRackIdContainersContainerIdMountedDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const freeTextRESTfulFreeTextPostBodyParam = {
      parent_diagram: {
        href: 'string'
      },
      creator: {
        href: 'string'
      },
      master_free_text: {
        href: 'string'
      },
      value: 'string',
      displayed_value: 'string',
      x: 9,
      y: 7,
      z_order: 2,
      is_alias: true,
      font_size: 5,
      color: 'string',
      background_color: 'string',
      justification: 6,
      can_move: false,
      angle: 10,
      is_displayed: true,
      is_bold: false,
      is_italic: true,
      is_underline: false,
      font_family: 'string',
      align: 3,
      href: 'string'
    };
    describe('#rESTfulFreeTextPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulFreeTextPost(freeTextRESTfulFreeTextPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FreeText', 'rESTfulFreeTextPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const freeTextMasterFreeTextId = 555;
    const freeTextPostWapiV1FreeTextMasterFreeTextIdAliasesBodyParam = {
      parent_diagram: {
        href: 'string'
      },
      creator: {
        href: 'string'
      },
      master_free_text: {
        href: 'string'
      },
      value: 'string',
      displayed_value: 'string',
      x: 5,
      y: 9,
      z_order: 4,
      is_alias: false,
      font_size: 2,
      color: 'string',
      background_color: 'string',
      justification: 7,
      can_move: false,
      angle: 4,
      is_displayed: false,
      is_bold: false,
      is_italic: false,
      is_underline: false,
      font_family: 'string',
      align: 4,
      href: 'string'
    };
    describe('#postWapiV1FreeTextMasterFreeTextIdAliases - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postWapiV1FreeTextMasterFreeTextIdAliases(freeTextMasterFreeTextId, freeTextPostWapiV1FreeTextMasterFreeTextIdAliasesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FreeText', 'postWapiV1FreeTextMasterFreeTextIdAliases', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const freeTextRESTfulFreeTextPutBodyParam = {
      parent_diagram: {
        href: 'string'
      },
      creator: {
        href: 'string'
      },
      master_free_text: {
        href: 'string'
      },
      value: 'string',
      displayed_value: 'string',
      x: 2,
      y: 6,
      z_order: 2,
      is_alias: false,
      font_size: 2,
      color: 'string',
      background_color: 'string',
      justification: 4,
      can_move: false,
      angle: 3,
      is_displayed: true,
      is_bold: true,
      is_italic: false,
      is_underline: false,
      font_family: 'string',
      align: 10,
      href: 'string'
    };
    describe('#rESTfulFreeTextPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulFreeTextPut(freeTextRESTfulFreeTextPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FreeText', 'rESTfulFreeTextPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulFreeTextGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulFreeTextGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FreeText', 'rESTfulFreeTextGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const freeTextId = 555;
    describe('#rESTfulFreeTextGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulFreeTextGetById(freeTextId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_diagram);
                assert.equal('object', typeof data.response.creator);
                assert.equal('object', typeof data.response.master_free_text);
                assert.equal('string', data.response.value);
                assert.equal('string', data.response.displayed_value);
                assert.equal(1, data.response.x);
                assert.equal(3, data.response.y);
                assert.equal(1, data.response.z_order);
                assert.equal(true, data.response.is_alias);
                assert.equal(1, data.response.font_size);
                assert.equal('string', data.response.color);
                assert.equal('string', data.response.background_color);
                assert.equal(3, data.response.justification);
                assert.equal(true, data.response.can_move);
                assert.equal(9, data.response.angle);
                assert.equal(false, data.response.is_displayed);
                assert.equal(true, data.response.is_bold);
                assert.equal(false, data.response.is_italic);
                assert.equal(true, data.response.is_underline);
                assert.equal('string', data.response.font_family);
                assert.equal(8, data.response.align);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FreeText', 'rESTfulFreeTextGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulFreeTextGetAliases - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulFreeTextGetAliases(freeTextMasterFreeTextId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FreeText', 'rESTfulFreeTextGetAliases', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userGroupsRESTfulGroupsPostBodyParam = {
      name: 'string',
      is_system: false,
      role: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#rESTfulGroupsPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulGroupsPost(userGroupsRESTfulGroupsPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'rESTfulGroupsPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userGroupsRESTfulGroupsPutBodyParam = {
      name: 'string',
      is_system: true,
      role: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#rESTfulGroupsPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulGroupsPut(userGroupsRESTfulGroupsPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'rESTfulGroupsPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulGroupsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulGroupsGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'rESTfulGroupsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userGroupsId = 555;
    const userGroupsPutWapiV1GroupsIdBodyParam = {
      name: 'string',
      is_system: false,
      role: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#putWapiV1GroupsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1GroupsId(userGroupsId, userGroupsPutWapiV1GroupsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'putWapiV1GroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulGroupsGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulGroupsGetById(userGroupsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.is_system);
                assert.equal('object', typeof data.response.role);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'rESTfulGroupsGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulHierarchyGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulHierarchyGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesHierarchy', 'rESTfulHierarchyGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesHierarchyId = 555;
    describe('#rESTfulHierarchyGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulHierarchyGetById(nodesHierarchyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesHierarchy', 'rESTfulHierarchyGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDeviceQueryImportImportDeviceQuery - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulDeviceQueryImportImportDeviceQuery((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImportDeviceTypeQuery', 'rESTfulDeviceQueryImportImportDeviceQuery', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkCategoriesRESTfulLinkCategoriesPostBodyParam = {
      parent_link_category: {
        href: 'string'
      },
      name: 'string',
      image: 'string',
      is_favorite: false,
      href: 'string'
    };
    describe('#rESTfulLinkCategoriesPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulLinkCategoriesPost(linkCategoriesRESTfulLinkCategoriesPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkCategories', 'rESTfulLinkCategoriesPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkCategoriesRESTfulLinkCategoriesPutBodyParam = {
      parent_link_category: {
        href: 'string'
      },
      name: 'string',
      image: 'string',
      is_favorite: false,
      href: 'string'
    };
    describe('#rESTfulLinkCategoriesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulLinkCategoriesPut(linkCategoriesRESTfulLinkCategoriesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkCategories', 'rESTfulLinkCategoriesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkCategoriesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulLinkCategoriesGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkCategories', 'rESTfulLinkCategoriesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkCategoriesId = 555;
    const linkCategoriesPutWapiV1LinkCategoriesIdBodyParam = {
      parent_link_category: {
        href: 'string'
      },
      name: 'string',
      image: 'string',
      is_favorite: false,
      href: 'string'
    };
    describe('#putWapiV1LinkCategoriesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1LinkCategoriesId(linkCategoriesId, linkCategoriesPutWapiV1LinkCategoriesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkCategories', 'putWapiV1LinkCategoriesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkCategoriesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulLinkCategoriesGetById(linkCategoriesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_link_category);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.image);
                assert.equal(false, data.response.is_favorite);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkCategories', 'rESTfulLinkCategoriesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkTypesPropertiesOverridesRESTfulLinkOverridesPostBodyParam = {
      parent_link_type_property: {
        href: 'string'
      },
      value: 'string',
      rule: 0,
      is_override: false,
      thickness: 10,
      link_style: 2,
      start_arrow: 9,
      end_arrow: 10,
      color: 'string',
      href: 'string'
    };
    describe('#rESTfulLinkOverridesPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulLinkOverridesPost(linkTypesPropertiesOverridesRESTfulLinkOverridesPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesPropertiesOverrides', 'rESTfulLinkOverridesPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkTypesPropertiesOverridesLinkTypePropertyId = 555;
    const linkTypesPropertiesOverridesLinkTypeId = 555;
    const linkTypesPropertiesOverridesPostWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesBodyParam = {
      parent_link_type_property: {
        href: 'string'
      },
      value: 'string',
      rule: 0,
      is_override: true,
      thickness: 3,
      link_style: 1,
      start_arrow: 0,
      end_arrow: 14,
      color: 'string',
      href: 'string'
    };
    describe('#postWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides(linkTypesPropertiesOverridesLinkTypeId, linkTypesPropertiesOverridesLinkTypePropertyId, linkTypesPropertiesOverridesPostWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesPropertiesOverrides', 'postWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkTypesPropertiesOverridesRESTfulLinkOverridesPutBodyParam = {
      parent_link_type_property: {
        href: 'string'
      },
      value: 'string',
      rule: -1,
      is_override: true,
      thickness: 8,
      link_style: 2,
      start_arrow: 7,
      end_arrow: 1,
      color: 'string',
      href: 'string'
    };
    describe('#rESTfulLinkOverridesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulLinkOverridesPut(linkTypesPropertiesOverridesRESTfulLinkOverridesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesPropertiesOverrides', 'rESTfulLinkOverridesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkOverridesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulLinkOverridesGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesPropertiesOverrides', 'rESTfulLinkOverridesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkTypesPropertiesOverridesId = 555;
    describe('#rESTfulLinkOverridesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulLinkOverridesGetById(linkTypesPropertiesOverridesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_link_type_property);
                assert.equal('string', data.response.value);
                assert.equal(1, data.response.rule);
                assert.equal(true, data.response.is_override);
                assert.equal(7, data.response.thickness);
                assert.equal(4, data.response.link_style);
                assert.equal(0, data.response.start_arrow);
                assert.equal(6, data.response.end_arrow);
                assert.equal('string', data.response.color);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesPropertiesOverrides', 'rESTfulLinkOverridesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverrides - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverrides(linkTypesPropertiesOverridesLinkTypePropertyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesPropertiesOverrides', 'getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverrides', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverridesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverridesId(linkTypesPropertiesOverridesLinkTypePropertyId, linkTypesPropertiesOverridesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_link_type_property);
                assert.equal('string', data.response.value);
                assert.equal(0, data.response.rule);
                assert.equal(false, data.response.is_override);
                assert.equal(10, data.response.thickness);
                assert.equal(2, data.response.link_style);
                assert.equal(5, data.response.start_arrow);
                assert.equal(14, data.response.end_arrow);
                assert.equal('string', data.response.color);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesPropertiesOverrides', 'getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverridesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides(linkTypesPropertiesOverridesLinkTypeId, linkTypesPropertiesOverridesLinkTypePropertyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesPropertiesOverrides', 'getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkTypesPropertiesOverridesPutWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesIdBodyParam = {
      parent_link_type_property: {
        href: 'string'
      },
      value: 'string',
      rule: -1,
      is_override: false,
      thickness: 6,
      link_style: 2,
      start_arrow: -2,
      end_arrow: 7,
      color: 'string',
      href: 'string'
    };
    describe('#putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId(linkTypesPropertiesOverridesId, linkTypesPropertiesOverridesLinkTypeId, linkTypesPropertiesOverridesLinkTypePropertyId, linkTypesPropertiesOverridesPutWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesPropertiesOverrides', 'putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId(linkTypesPropertiesOverridesLinkTypeId, linkTypesPropertiesOverridesLinkTypePropertyId, linkTypesPropertiesOverridesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_link_type_property);
                assert.equal('string', data.response.value);
                assert.equal(1, data.response.rule);
                assert.equal(false, data.response.is_override);
                assert.equal(8, data.response.thickness);
                assert.equal(1, data.response.link_style);
                assert.equal(7, data.response.start_arrow);
                assert.equal(15, data.response.end_arrow);
                assert.equal('string', data.response.color);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesPropertiesOverrides', 'getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linksPropertyValuesLinkId = 555;
    describe('#rESTfulLinkPropertyValuesInitAbsentValues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulLinkPropertyValuesInitAbsentValues(linksPropertyValuesLinkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinksPropertyValues', 'rESTfulLinkPropertyValuesInitAbsentValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linksPropertyValuesRESTfulLinkPropertyValuesPutBodyParam = {};
    describe('#rESTfulLinkPropertyValuesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulLinkPropertyValuesPut(linksPropertyValuesRESTfulLinkPropertyValuesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinksPropertyValues', 'rESTfulLinkPropertyValuesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkPropertyValuesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulLinkPropertyValuesGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinksPropertyValues', 'rESTfulLinkPropertyValuesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linksPropertyValuesId = 555;
    describe('#rESTfulLinkPropertyValuesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulLinkPropertyValuesGetById(linksPropertyValuesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.parent_link);
                assert.equal('object', typeof data.response.link_type_property);
                assert.equal('string', data.response.value);
                assert.equal('string', data.response.displayed_value);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinksPropertyValues', 'rESTfulLinkPropertyValuesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linksPropertyValuesPutWapiV1LinksLinkIdLinkPropertyValuesBodyParam = {
      name: 'string',
      parent_link: {
        href: 'string'
      },
      link_type_property: {
        href: 'string'
      },
      value: 'string',
      displayed_value: 'string',
      href: 'string'
    };
    describe('#putWapiV1LinksLinkIdLinkPropertyValues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1LinksLinkIdLinkPropertyValues(linksPropertyValuesLinkId, linksPropertyValuesPutWapiV1LinksLinkIdLinkPropertyValuesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinksPropertyValues', 'putWapiV1LinksLinkIdLinkPropertyValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1LinksLinkIdLinkPropertyValues - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1LinksLinkIdLinkPropertyValues(linksPropertyValuesLinkId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinksPropertyValues', 'getWapiV1LinksLinkIdLinkPropertyValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linksPropertyValuesPutWapiV1LinksLinkIdLinkPropertyValuesIdBodyParam = {
      name: 'string',
      parent_link: {
        href: 'string'
      },
      link_type_property: {
        href: 'string'
      },
      value: 'string',
      displayed_value: 'string',
      href: 'string'
    };
    describe('#putWapiV1LinksLinkIdLinkPropertyValuesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1LinksLinkIdLinkPropertyValuesId(linksPropertyValuesId, linksPropertyValuesLinkId, linksPropertyValuesPutWapiV1LinksLinkIdLinkPropertyValuesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinksPropertyValues', 'putWapiV1LinksLinkIdLinkPropertyValuesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1LinksLinkIdLinkPropertyValuesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1LinksLinkIdLinkPropertyValuesId(linksPropertyValuesLinkId, linksPropertyValuesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.parent_link);
                assert.equal('object', typeof data.response.link_type_property);
                assert.equal('string', data.response.value);
                assert.equal('string', data.response.displayed_value);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinksPropertyValues', 'getWapiV1LinksLinkIdLinkPropertyValuesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkTypesPropertiesRESTfulLinkTypePropertiesPostBodyParam = {
      name: 'string',
      parent_link_type: {
        href: 'string'
      },
      link_overrides: {
        href: 'string'
      },
      is_system: false,
      default_value: 'string',
      position: 2,
      is_mandatory: true,
      is_displayed: false,
      is_in_properties: false,
      text_justification: 1,
      text_align: 0,
      font_family: 11,
      font_size: 7,
      font_color: 'string',
      fill_color: 'string',
      offset_x: 4,
      offset_y: 4,
      angle: 7,
      is_bold: false,
      is_italic: false,
      is_underlined: true,
      is_lock_list: false,
      is_type_field: true,
      is_unique_for_this_type: true,
      is_unique_for_all_types: true,
      is_upright_alignment: false,
      anchor: 2,
      href: 'string'
    };
    describe('#rESTfulLinkTypePropertiesPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulLinkTypePropertiesPost(linkTypesPropertiesRESTfulLinkTypePropertiesPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesProperties', 'rESTfulLinkTypePropertiesPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkTypesPropertiesLinkTypeId = 555;
    const linkTypesPropertiesPostWapiV1LinkTypesLinkTypeIdLinkTypePropertiesBodyParam = {
      name: 'string',
      parent_link_type: {
        href: 'string'
      },
      link_overrides: {
        href: 'string'
      },
      is_system: false,
      default_value: 'string',
      position: 2,
      is_mandatory: false,
      is_displayed: false,
      is_in_properties: false,
      text_justification: 1,
      text_align: 2,
      font_family: 5,
      font_size: 6,
      font_color: 'string',
      fill_color: 'string',
      offset_x: 8,
      offset_y: 2,
      angle: 1,
      is_bold: false,
      is_italic: false,
      is_underlined: false,
      is_lock_list: false,
      is_type_field: true,
      is_unique_for_this_type: false,
      is_unique_for_all_types: true,
      is_upright_alignment: true,
      anchor: 1,
      href: 'string'
    };
    describe('#postWapiV1LinkTypesLinkTypeIdLinkTypeProperties - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postWapiV1LinkTypesLinkTypeIdLinkTypeProperties(linkTypesPropertiesLinkTypeId, linkTypesPropertiesPostWapiV1LinkTypesLinkTypeIdLinkTypePropertiesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesProperties', 'postWapiV1LinkTypesLinkTypeIdLinkTypeProperties', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkTypesPropertiesRESTfulLinkTypePropertiesPutBodyParam = {};
    describe('#rESTfulLinkTypePropertiesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulLinkTypePropertiesPut(linkTypesPropertiesRESTfulLinkTypePropertiesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesProperties', 'rESTfulLinkTypePropertiesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkTypePropertiesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulLinkTypePropertiesGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesProperties', 'rESTfulLinkTypePropertiesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkTypesPropertiesId = 555;
    describe('#rESTfulLinkTypePropertiesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulLinkTypePropertiesGetById(linkTypesPropertiesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.parent_link_type);
                assert.equal('object', typeof data.response.link_overrides);
                assert.equal(false, data.response.is_system);
                assert.equal('string', data.response.default_value);
                assert.equal(1, data.response.position);
                assert.equal(false, data.response.is_mandatory);
                assert.equal(false, data.response.is_displayed);
                assert.equal(true, data.response.is_in_properties);
                assert.equal(1, data.response.text_justification);
                assert.equal(1, data.response.text_align);
                assert.equal(12, data.response.font_family);
                assert.equal(8, data.response.font_size);
                assert.equal('string', data.response.font_color);
                assert.equal('string', data.response.fill_color);
                assert.equal(4, data.response.offset_x);
                assert.equal(10, data.response.offset_y);
                assert.equal(9, data.response.angle);
                assert.equal(true, data.response.is_bold);
                assert.equal(false, data.response.is_italic);
                assert.equal(true, data.response.is_underlined);
                assert.equal(true, data.response.is_lock_list);
                assert.equal(false, data.response.is_type_field);
                assert.equal(false, data.response.is_unique_for_this_type);
                assert.equal(false, data.response.is_unique_for_all_types);
                assert.equal(true, data.response.is_upright_alignment);
                assert.equal(1, data.response.anchor);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesProperties', 'rESTfulLinkTypePropertiesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkTypesPropertiesPutWapiV1LinkTypesLinkTypeIdLinkTypePropertiesBodyParam = {};
    describe('#putWapiV1LinkTypesLinkTypeIdLinkTypeProperties - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1LinkTypesLinkTypeIdLinkTypeProperties(linkTypesPropertiesLinkTypeId, linkTypesPropertiesPutWapiV1LinkTypesLinkTypeIdLinkTypePropertiesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesProperties', 'putWapiV1LinkTypesLinkTypeIdLinkTypeProperties', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1LinkTypesLinkTypeIdLinkTypeProperties - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1LinkTypesLinkTypeIdLinkTypeProperties(linkTypesPropertiesLinkTypeId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesProperties', 'getWapiV1LinkTypesLinkTypeIdLinkTypeProperties', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkTypesPropertiesPutWapiV1LinkTypesLinkTypeIdLinkTypePropertiesIdBodyParam = {
      name: 'string',
      parent_link_type: {
        href: 'string'
      },
      link_overrides: {
        href: 'string'
      },
      is_system: false,
      default_value: 'string',
      position: 3,
      is_mandatory: true,
      is_displayed: false,
      is_in_properties: false,
      text_justification: 0,
      text_align: 0,
      font_family: 3,
      font_size: 7,
      font_color: 'string',
      fill_color: 'string',
      offset_x: 5,
      offset_y: 10,
      angle: 8,
      is_bold: true,
      is_italic: true,
      is_underlined: true,
      is_lock_list: false,
      is_type_field: false,
      is_unique_for_this_type: false,
      is_unique_for_all_types: true,
      is_upright_alignment: true,
      anchor: 0,
      href: 'string'
    };
    describe('#putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId(linkTypesPropertiesId, linkTypesPropertiesLinkTypeId, linkTypesPropertiesPutWapiV1LinkTypesLinkTypeIdLinkTypePropertiesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesProperties', 'putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId(linkTypesPropertiesLinkTypeId, linkTypesPropertiesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.parent_link_type);
                assert.equal('object', typeof data.response.link_overrides);
                assert.equal(false, data.response.is_system);
                assert.equal('string', data.response.default_value);
                assert.equal(2, data.response.position);
                assert.equal(false, data.response.is_mandatory);
                assert.equal(true, data.response.is_displayed);
                assert.equal(true, data.response.is_in_properties);
                assert.equal(1, data.response.text_justification);
                assert.equal(0, data.response.text_align);
                assert.equal(5, data.response.font_family);
                assert.equal(9, data.response.font_size);
                assert.equal('string', data.response.font_color);
                assert.equal('string', data.response.fill_color);
                assert.equal(8, data.response.offset_x);
                assert.equal(7, data.response.offset_y);
                assert.equal(6, data.response.angle);
                assert.equal(false, data.response.is_bold);
                assert.equal(true, data.response.is_italic);
                assert.equal(false, data.response.is_underlined);
                assert.equal(false, data.response.is_lock_list);
                assert.equal(false, data.response.is_type_field);
                assert.equal(true, data.response.is_unique_for_this_type);
                assert.equal(true, data.response.is_unique_for_all_types);
                assert.equal(false, data.response.is_upright_alignment);
                assert.equal(0, data.response.anchor);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesProperties', 'getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkTypesRESTfulLinkTypesPostBodyParam = {
      name: 'string',
      is_favorite: false,
      is_system: true,
      is_enabled: false,
      is_snapped_to_edge: false,
      is_matching_port_connectors: true,
      thickness: 6,
      link_style: 2,
      start_arrow: 15,
      end_arrow: 2,
      color: 'string',
      link_category: {
        href: 'string'
      },
      link_type_properties: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#rESTfulLinkTypesPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulLinkTypesPost(linkTypesRESTfulLinkTypesPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypes', 'rESTfulLinkTypesPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkTypesRESTfulLinkTypesPutBodyParam = {
      name: 'string',
      is_favorite: true,
      is_system: true,
      is_enabled: false,
      is_snapped_to_edge: false,
      is_matching_port_connectors: true,
      thickness: 3,
      link_style: 3,
      start_arrow: 2,
      end_arrow: 12,
      color: 'string',
      link_category: {
        href: 'string'
      },
      link_type_properties: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#rESTfulLinkTypesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulLinkTypesPut(linkTypesRESTfulLinkTypesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypes', 'rESTfulLinkTypesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkTypesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulLinkTypesGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypes', 'rESTfulLinkTypesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkTypesId = 555;
    const linkTypesPutWapiV1LinkTypesIdBodyParam = {
      name: 'string',
      is_favorite: true,
      is_system: false,
      is_enabled: true,
      is_snapped_to_edge: false,
      is_matching_port_connectors: false,
      thickness: 2,
      link_style: 2,
      start_arrow: 3,
      end_arrow: 9,
      color: 'string',
      link_category: {
        href: 'string'
      },
      link_type_properties: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#putWapiV1LinkTypesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1LinkTypesId(linkTypesId, linkTypesPutWapiV1LinkTypesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypes', 'putWapiV1LinkTypesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkTypesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulLinkTypesGetById(linkTypesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.is_favorite);
                assert.equal(true, data.response.is_system);
                assert.equal(true, data.response.is_enabled);
                assert.equal(false, data.response.is_snapped_to_edge);
                assert.equal(true, data.response.is_matching_port_connectors);
                assert.equal(6, data.response.thickness);
                assert.equal(0, data.response.link_style);
                assert.equal(2, data.response.start_arrow);
                assert.equal(2, data.response.end_arrow);
                assert.equal('string', data.response.color);
                assert.equal('object', typeof data.response.link_category);
                assert.equal('object', typeof data.response.link_type_properties);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypes', 'rESTfulLinkTypesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linksVisLinksLinkId = 'fakedata';
    const linksVisLinksRESTfulVisLinksPutBodyParam = {
      parent_diagram: {
        href: 'string'
      },
      displayed_fields: {
        href: 'string'
      },
      is_curved: true,
      bend_points_path: 'string',
      from_anchor_point_x: 1,
      from_anchor_point_y: 10,
      to_anchor_point_x: 2,
      to_anchor_point_y: 10,
      can_move: false,
      can_delete: true,
      is_hidden: true,
      is_snapped_to_edge: false,
      z_order: 3,
      href: 'string'
    };
    describe('#rESTfulVisLinksPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulVisLinksPut(linksVisLinksRESTfulVisLinksPutBodyParam, linksVisLinksLinkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinksVisLinks', 'rESTfulVisLinksPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisLinksGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulVisLinksGet(linksVisLinksLinkId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinksVisLinks', 'rESTfulVisLinksGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linksVisLinksId = 555;
    describe('#rESTfulVisLinksGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulVisLinksGetById(linksVisLinksLinkId, linksVisLinksId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_diagram);
                assert.equal('object', typeof data.response.displayed_fields);
                assert.equal(true, data.response.is_curved);
                assert.equal('string', data.response.bend_points_path);
                assert.equal(9, data.response.from_anchor_point_x);
                assert.equal(7, data.response.from_anchor_point_y);
                assert.equal(3, data.response.to_anchor_point_x);
                assert.equal(5, data.response.to_anchor_point_y);
                assert.equal(true, data.response.can_move);
                assert.equal(false, data.response.can_delete);
                assert.equal(false, data.response.is_hidden);
                assert.equal(false, data.response.is_snapped_to_edge);
                assert.equal(10, data.response.z_order);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinksVisLinks', 'rESTfulVisLinksGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeCategoriesRESTfulNodeCategoriesPostBodyParam = {
      parent_node_category: {
        href: 'string'
      },
      name: 'string',
      image: 'string',
      node_type_group: 12,
      is_favorite: false,
      href: 'string'
    };
    describe('#rESTfulNodeCategoriesPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodeCategoriesPost(nodeCategoriesRESTfulNodeCategoriesPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeCategories', 'rESTfulNodeCategoriesPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeCategoriesRESTfulNodeCategoriesPutBodyParam = {
      parent_node_category: {
        href: 'string'
      },
      name: 'string',
      image: 'string',
      node_type_group: 10,
      is_favorite: false,
      href: 'string'
    };
    describe('#rESTfulNodeCategoriesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodeCategoriesPut(nodeCategoriesRESTfulNodeCategoriesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeCategories', 'rESTfulNodeCategoriesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeCategoriesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulNodeCategoriesGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeCategories', 'rESTfulNodeCategoriesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeCategoriesId = 555;
    const nodeCategoriesPutWapiV1NodeCategoriesIdBodyParam = {
      parent_node_category: {
        href: 'string'
      },
      name: 'string',
      image: 'string',
      node_type_group: 12,
      is_favorite: true,
      href: 'string'
    };
    describe('#putWapiV1NodeCategoriesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1NodeCategoriesId(nodeCategoriesId, nodeCategoriesPutWapiV1NodeCategoriesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeCategories', 'putWapiV1NodeCategoriesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeCategoriesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulNodeCategoriesGetById(nodeCategoriesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_node_category);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.image);
                assert.equal(10, data.response.node_type_group);
                assert.equal(false, data.response.is_favorite);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeCategories', 'rESTfulNodeCategoriesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesPropertiesOverridesRESTfulNodeOverridesPostBodyParam = {
      parent_node_type_property: {
        href: 'string'
      },
      value: 'string',
      rule: 4,
      is_override: true,
      image: 'string',
      instance_effect: 5,
      parent_effect: 0,
      upwards_propagation: 3,
      href: 'string'
    };
    describe('#rESTfulNodeOverridesPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodeOverridesPost(nodeTypesPropertiesOverridesRESTfulNodeOverridesPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPropertiesOverrides', 'rESTfulNodeOverridesPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesPropertiesOverridesNodeTypePropertyId = 555;
    const nodeTypesPropertiesOverridesNodeTypeId = 555;
    const nodeTypesPropertiesOverridesPostWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesBodyParam = {
      parent_node_type_property: {
        href: 'string'
      },
      value: 'string',
      rule: 1,
      is_override: true,
      image: 'string',
      instance_effect: 1,
      parent_effect: 4,
      upwards_propagation: 2,
      href: 'string'
    };
    describe('#postWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides(nodeTypesPropertiesOverridesNodeTypeId, nodeTypesPropertiesOverridesNodeTypePropertyId, nodeTypesPropertiesOverridesPostWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPropertiesOverrides', 'postWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesPropertiesOverridesRESTfulNodeOverridesPutBodyParam = {
      parent_node_type_property: {
        href: 'string'
      },
      value: 'string',
      rule: 4,
      is_override: true,
      image: 'string',
      instance_effect: 4,
      parent_effect: 5,
      upwards_propagation: 2,
      href: 'string'
    };
    describe('#rESTfulNodeOverridesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodeOverridesPut(nodeTypesPropertiesOverridesRESTfulNodeOverridesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPropertiesOverrides', 'rESTfulNodeOverridesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeOverridesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulNodeOverridesGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPropertiesOverrides', 'rESTfulNodeOverridesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesPropertiesOverridesId = 555;
    describe('#rESTfulNodeOverridesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulNodeOverridesGetById(nodeTypesPropertiesOverridesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_node_type_property);
                assert.equal('string', data.response.value);
                assert.equal(1, data.response.rule);
                assert.equal(false, data.response.is_override);
                assert.equal('string', data.response.image);
                assert.equal(4, data.response.instance_effect);
                assert.equal(1, data.response.parent_effect);
                assert.equal(0, data.response.upwards_propagation);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPropertiesOverrides', 'rESTfulNodeOverridesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverrides - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverrides(nodeTypesPropertiesOverridesNodeTypePropertyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPropertiesOverrides', 'getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverrides', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverridesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverridesId(nodeTypesPropertiesOverridesNodeTypePropertyId, nodeTypesPropertiesOverridesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_node_type_property);
                assert.equal('string', data.response.value);
                assert.equal(-1, data.response.rule);
                assert.equal(false, data.response.is_override);
                assert.equal('string', data.response.image);
                assert.equal(0, data.response.instance_effect);
                assert.equal(4, data.response.parent_effect);
                assert.equal(3, data.response.upwards_propagation);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPropertiesOverrides', 'getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverridesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides(nodeTypesPropertiesOverridesNodeTypeId, nodeTypesPropertiesOverridesNodeTypePropertyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPropertiesOverrides', 'getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesPropertiesOverridesPutWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesIdBodyParam = {
      parent_node_type_property: {
        href: 'string'
      },
      value: 'string',
      rule: -1,
      is_override: true,
      image: 'string',
      instance_effect: 2,
      parent_effect: 0,
      upwards_propagation: 4,
      href: 'string'
    };
    describe('#putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId(nodeTypesPropertiesOverridesId, nodeTypesPropertiesOverridesNodeTypeId, nodeTypesPropertiesOverridesNodeTypePropertyId, nodeTypesPropertiesOverridesPutWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPropertiesOverrides', 'putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId(nodeTypesPropertiesOverridesNodeTypeId, nodeTypesPropertiesOverridesNodeTypePropertyId, nodeTypesPropertiesOverridesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_node_type_property);
                assert.equal('string', data.response.value);
                assert.equal(0, data.response.rule);
                assert.equal(false, data.response.is_override);
                assert.equal('string', data.response.image);
                assert.equal(1, data.response.instance_effect);
                assert.equal(0, data.response.parent_effect);
                assert.equal(0, data.response.upwards_propagation);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPropertiesOverrides', 'getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesPropertyValuesNodeId = 555;
    describe('#rESTfulNodePropertyValuesInitAbsentValues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodePropertyValuesInitAbsentValues(nodesPropertyValuesNodeId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesPropertyValues', 'rESTfulNodePropertyValuesInitAbsentValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesPropertyValuesRESTfulNodePropertyValuesPutBodyParam = {};
    describe('#rESTfulNodePropertyValuesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodePropertyValuesPut(nodesPropertyValuesRESTfulNodePropertyValuesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesPropertyValues', 'rESTfulNodePropertyValuesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodePropertyValuesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulNodePropertyValuesGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesPropertyValues', 'rESTfulNodePropertyValuesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesPropertyValuesId = 555;
    describe('#rESTfulNodePropertyValuesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulNodePropertyValuesGetById(nodesPropertyValuesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.parent_node);
                assert.equal('object', typeof data.response.node_type_property);
                assert.equal('string', data.response.value);
                assert.equal('string', data.response.displayed_value);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesPropertyValues', 'rESTfulNodePropertyValuesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesPropertyValuesPutWapiV1NodesNodeIdNodePropertyValuesBodyParam = {
      name: 'string',
      parent_node: {
        href: 'string'
      },
      node_type_property: {
        href: 'string'
      },
      value: 'string',
      displayed_value: 'string',
      href: 'string'
    };
    describe('#putWapiV1NodesNodeIdNodePropertyValues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1NodesNodeIdNodePropertyValues(nodesPropertyValuesNodeId, nodesPropertyValuesPutWapiV1NodesNodeIdNodePropertyValuesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesPropertyValues', 'putWapiV1NodesNodeIdNodePropertyValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1NodesNodeIdNodePropertyValues - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1NodesNodeIdNodePropertyValues(nodesPropertyValuesNodeId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesPropertyValues', 'getWapiV1NodesNodeIdNodePropertyValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesPropertyValuesPutWapiV1NodesNodeIdNodePropertyValuesIdBodyParam = {
      name: 'string',
      parent_node: {
        href: 'string'
      },
      node_type_property: {
        href: 'string'
      },
      value: 'string',
      displayed_value: 'string',
      href: 'string'
    };
    describe('#putWapiV1NodesNodeIdNodePropertyValuesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1NodesNodeIdNodePropertyValuesId(nodesPropertyValuesId, nodesPropertyValuesNodeId, nodesPropertyValuesPutWapiV1NodesNodeIdNodePropertyValuesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesPropertyValues', 'putWapiV1NodesNodeIdNodePropertyValuesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1NodesNodeIdNodePropertyValuesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1NodesNodeIdNodePropertyValuesId(nodesPropertyValuesNodeId, nodesPropertyValuesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.parent_node);
                assert.equal('object', typeof data.response.node_type_property);
                assert.equal('string', data.response.value);
                assert.equal('string', data.response.displayed_value);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesPropertyValues', 'getWapiV1NodesNodeIdNodePropertyValuesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesPropertiesRESTfulNodeTypePropertiesPostBodyParam = {
      name: 'string',
      parent_node_type: {
        href: 'string'
      },
      node_overrides: {
        href: 'string'
      },
      is_system: true,
      default_value: 'string',
      position: 5,
      is_mandatory: true,
      is_displayed: true,
      is_in_properties: false,
      text_justification: 1,
      text_align: 2,
      font_family: 8,
      font_size: 8,
      font_color: 'string',
      fill_color: 'string',
      offset_x: 7,
      offset_y: 8,
      angle: 7,
      is_bold: false,
      is_italic: true,
      is_underlined: false,
      is_lock_list: true,
      is_type_field: true,
      is_unique_for_this_type: false,
      is_unique_for_all_types: true,
      is_not_editable: false,
      system_object_id: 'string',
      href: 'string'
    };
    describe('#rESTfulNodeTypePropertiesPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodeTypePropertiesPost(nodeTypesPropertiesRESTfulNodeTypePropertiesPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesProperties', 'rESTfulNodeTypePropertiesPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesPropertiesNodeTypeId = 555;
    const nodeTypesPropertiesPostWapiV1NodeTypesNodeTypeIdNodeTypePropertiesBodyParam = {
      name: 'string',
      parent_node_type: {
        href: 'string'
      },
      node_overrides: {
        href: 'string'
      },
      is_system: true,
      default_value: 'string',
      position: 7,
      is_mandatory: false,
      is_displayed: true,
      is_in_properties: false,
      text_justification: 1,
      text_align: 2,
      font_family: 5,
      font_size: 3,
      font_color: 'string',
      fill_color: 'string',
      offset_x: 6,
      offset_y: 7,
      angle: 4,
      is_bold: false,
      is_italic: true,
      is_underlined: false,
      is_lock_list: true,
      is_type_field: false,
      is_unique_for_this_type: true,
      is_unique_for_all_types: true,
      is_not_editable: false,
      system_object_id: 'string',
      href: 'string'
    };
    describe('#postWapiV1NodeTypesNodeTypeIdNodeTypeProperties - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postWapiV1NodeTypesNodeTypeIdNodeTypeProperties(nodeTypesPropertiesNodeTypeId, nodeTypesPropertiesPostWapiV1NodeTypesNodeTypeIdNodeTypePropertiesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesProperties', 'postWapiV1NodeTypesNodeTypeIdNodeTypeProperties', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesPropertiesRESTfulNodeTypePropertiesPutBodyParam = {};
    describe('#rESTfulNodeTypePropertiesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodeTypePropertiesPut(nodeTypesPropertiesRESTfulNodeTypePropertiesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesProperties', 'rESTfulNodeTypePropertiesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeTypePropertiesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulNodeTypePropertiesGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesProperties', 'rESTfulNodeTypePropertiesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesPropertiesId = 555;
    describe('#rESTfulNodeTypePropertiesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulNodeTypePropertiesGetById(nodeTypesPropertiesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.parent_node_type);
                assert.equal('object', typeof data.response.node_overrides);
                assert.equal(true, data.response.is_system);
                assert.equal('string', data.response.default_value);
                assert.equal(8, data.response.position);
                assert.equal(false, data.response.is_mandatory);
                assert.equal(true, data.response.is_displayed);
                assert.equal(false, data.response.is_in_properties);
                assert.equal(0, data.response.text_justification);
                assert.equal(2, data.response.text_align);
                assert.equal(2, data.response.font_family);
                assert.equal(8, data.response.font_size);
                assert.equal('string', data.response.font_color);
                assert.equal('string', data.response.fill_color);
                assert.equal(9, data.response.offset_x);
                assert.equal(10, data.response.offset_y);
                assert.equal(8, data.response.angle);
                assert.equal(false, data.response.is_bold);
                assert.equal(false, data.response.is_italic);
                assert.equal(false, data.response.is_underlined);
                assert.equal(false, data.response.is_lock_list);
                assert.equal(false, data.response.is_type_field);
                assert.equal(false, data.response.is_unique_for_this_type);
                assert.equal(true, data.response.is_unique_for_all_types);
                assert.equal(true, data.response.is_not_editable);
                assert.equal('string', data.response.system_object_id);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesProperties', 'rESTfulNodeTypePropertiesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesPropertiesPutWapiV1NodeTypesNodeTypeIdNodeTypePropertiesBodyParam = {};
    describe('#putWapiV1NodeTypesNodeTypeIdNodeTypeProperties - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1NodeTypesNodeTypeIdNodeTypeProperties(nodeTypesPropertiesNodeTypeId, nodeTypesPropertiesPutWapiV1NodeTypesNodeTypeIdNodeTypePropertiesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesProperties', 'putWapiV1NodeTypesNodeTypeIdNodeTypeProperties', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1NodeTypesNodeTypeIdNodeTypeProperties - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1NodeTypesNodeTypeIdNodeTypeProperties(nodeTypesPropertiesNodeTypeId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesProperties', 'getWapiV1NodeTypesNodeTypeIdNodeTypeProperties', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesPropertiesPutWapiV1NodeTypesNodeTypeIdNodeTypePropertiesIdBodyParam = {
      name: 'string',
      parent_node_type: {
        href: 'string'
      },
      node_overrides: {
        href: 'string'
      },
      is_system: false,
      default_value: 'string',
      position: 9,
      is_mandatory: false,
      is_displayed: true,
      is_in_properties: true,
      text_justification: 0,
      text_align: 0,
      font_family: 9,
      font_size: 4,
      font_color: 'string',
      fill_color: 'string',
      offset_x: 5,
      offset_y: 5,
      angle: 5,
      is_bold: false,
      is_italic: false,
      is_underlined: false,
      is_lock_list: true,
      is_type_field: false,
      is_unique_for_this_type: false,
      is_unique_for_all_types: true,
      is_not_editable: false,
      system_object_id: 'string',
      href: 'string'
    };
    describe('#putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId(nodeTypesPropertiesId, nodeTypesPropertiesNodeTypeId, nodeTypesPropertiesPutWapiV1NodeTypesNodeTypeIdNodeTypePropertiesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesProperties', 'putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId(nodeTypesPropertiesNodeTypeId, nodeTypesPropertiesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.parent_node_type);
                assert.equal('object', typeof data.response.node_overrides);
                assert.equal(true, data.response.is_system);
                assert.equal('string', data.response.default_value);
                assert.equal(9, data.response.position);
                assert.equal(false, data.response.is_mandatory);
                assert.equal(true, data.response.is_displayed);
                assert.equal(false, data.response.is_in_properties);
                assert.equal(0, data.response.text_justification);
                assert.equal(2, data.response.text_align);
                assert.equal(9, data.response.font_family);
                assert.equal(1, data.response.font_size);
                assert.equal('string', data.response.font_color);
                assert.equal('string', data.response.fill_color);
                assert.equal(10, data.response.offset_x);
                assert.equal(5, data.response.offset_y);
                assert.equal(10, data.response.angle);
                assert.equal(true, data.response.is_bold);
                assert.equal(true, data.response.is_italic);
                assert.equal(true, data.response.is_underlined);
                assert.equal(false, data.response.is_lock_list);
                assert.equal(true, data.response.is_type_field);
                assert.equal(true, data.response.is_unique_for_this_type);
                assert.equal(true, data.response.is_unique_for_all_types);
                assert.equal(false, data.response.is_not_editable);
                assert.equal('string', data.response.system_object_id);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesProperties', 'getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesRESTfulNodeTypesPostBodyParam = {
      name: 'string',
      image: 'string',
      default_height: 9,
      default_width: 10,
      node_type_group: 12,
      is_favorite: false,
      is_system: true,
      is_enabled: true,
      description: 'string',
      is_keep_aspect_ratio: false,
      double_click_behavior: 0,
      double_click_behavior_attribute: 'string',
      template: {
        href: 'string'
      },
      hierarchy_browser_label: {
        href: 'string'
      },
      node_category: {
        href: 'string'
      },
      node_type_properties: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#rESTfulNodeTypesPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodeTypesPost(nodeTypesRESTfulNodeTypesPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypes', 'rESTfulNodeTypesPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesRESTfulNodeTypesPutBodyParam = {
      name: 'string',
      image: 'string',
      default_height: 5,
      default_width: 8,
      node_type_group: 4,
      is_favorite: false,
      is_system: false,
      is_enabled: true,
      description: 'string',
      is_keep_aspect_ratio: false,
      double_click_behavior: 1,
      double_click_behavior_attribute: 'string',
      template: {
        href: 'string'
      },
      hierarchy_browser_label: {
        href: 'string'
      },
      node_category: {
        href: 'string'
      },
      node_type_properties: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#rESTfulNodeTypesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodeTypesPut(nodeTypesRESTfulNodeTypesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypes', 'rESTfulNodeTypesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeTypesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulNodeTypesGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypes', 'rESTfulNodeTypesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesId = 555;
    const nodeTypesPutWapiV1NodeTypesIdBodyParam = {
      name: 'string',
      image: 'string',
      default_height: 1,
      default_width: 1,
      node_type_group: 10,
      is_favorite: true,
      is_system: true,
      is_enabled: false,
      description: 'string',
      is_keep_aspect_ratio: true,
      double_click_behavior: 3,
      double_click_behavior_attribute: 'string',
      template: {
        href: 'string'
      },
      hierarchy_browser_label: {
        href: 'string'
      },
      node_category: {
        href: 'string'
      },
      node_type_properties: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#putWapiV1NodeTypesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1NodeTypesId(nodeTypesId, nodeTypesPutWapiV1NodeTypesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypes', 'putWapiV1NodeTypesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeTypesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulNodeTypesGetById(nodeTypesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.image);
                assert.equal(8, data.response.default_height);
                assert.equal(1, data.response.default_width);
                assert.equal(10, data.response.node_type_group);
                assert.equal(true, data.response.is_favorite);
                assert.equal(false, data.response.is_system);
                assert.equal(true, data.response.is_enabled);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.is_keep_aspect_ratio);
                assert.equal(2, data.response.double_click_behavior);
                assert.equal('string', data.response.double_click_behavior_attribute);
                assert.equal('object', typeof data.response.template);
                assert.equal('object', typeof data.response.hierarchy_browser_label);
                assert.equal('object', typeof data.response.node_category);
                assert.equal('object', typeof data.response.node_type_properties);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypes', 'rESTfulNodeTypesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesRESTfulNodesPostBodyParam = {};
    describe('#rESTfulNodesPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodesPost(nodesRESTfulNodesPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'rESTfulNodesPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesMasterNodeId = 555;
    const nodesRESTfulNodesPostAliasBodyParam = {};
    describe('#rESTfulNodesPostAlias - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodesPostAlias(nodesMasterNodeId, nodesRESTfulNodesPostAliasBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'rESTfulNodesPostAlias', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesRESTfulNodesPutBodyParam = {};
    describe('#rESTfulNodesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodesPut(nodesRESTfulNodesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'rESTfulNodesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulNodesGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'rESTfulNodesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesPutWapiV1NodesSeparateLinksBodyParam = {
      node_1: {
        href: 'string'
      },
      node_2: {
        href: 'string'
      },
      distance: 10,
      bend_points_number: 10,
      is_curved: true
    };
    describe('#putWapiV1NodesSeparateLinks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1NodesSeparateLinks(nodesPutWapiV1NodesSeparateLinksBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'putWapiV1NodesSeparateLinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesId = 555;
    const nodesPutWapiV1NodesIdBodyParam = {};
    describe('#putWapiV1NodesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1NodesId(nodesId, nodesPutWapiV1NodesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'putWapiV1NodesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulNodesGetById(nodesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.parent_diagram);
                assert.equal('object', typeof data.response.node_type);
                assert.equal('object', typeof data.response.creator);
                assert.equal('object', typeof data.response.master_node);
                assert.equal('object', typeof data.response.diagram_settings);
                assert.equal('object', typeof data.response.node_property_values);
                assert.equal('object', typeof data.response.vis_nodes);
                assert.equal('object', typeof data.response.map_coordinates);
                assert.equal('object', typeof data.response.aliases);
                assert.equal('string', data.response.double_click_behavior);
                assert.equal(false, data.response.is_alias);
                assert.equal('string', data.response.breadcrumbs);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'rESTfulNodesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodesGetAliases - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulNodesGetAliases(nodesMasterNodeId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'rESTfulNodesGetAliases', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesDiagramSettingsId = 555;
    const nodesDiagramSettingsRESTfulDiagramSettingsPutBodyParam = {
      url: 'string',
      background: 'string',
      is_template: false,
      template: {
        href: 'string'
      },
      page_color: 'string',
      diagram_width: 8,
      diagram_height: 8,
      diagram_margin_size: 2,
      is_blocked: true,
      visibility_layers_settings: 'string',
      auto_layout_state: 9,
      map_coord_left: 6,
      map_coord_top: 5,
      map_coord_right: 9,
      map_coord_bottom: 3,
      map_units_type: 10,
      map_source: 5,
      is_display_grid: true,
      is_snap_to_grid: false,
      grid_spacing_x: 3,
      grid_spacing_y: 2,
      read_only_for_non_admins: false,
      href: 'string'
    };
    describe('#rESTfulDiagramSettingsPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulDiagramSettingsPut(nodesDiagramSettingsId, nodesDiagramSettingsRESTfulDiagramSettingsPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesDiagramSettings', 'rESTfulDiagramSettingsPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDiagramSettingsGetDiagramSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulDiagramSettingsGetDiagramSettings(nodesDiagramSettingsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.background);
                assert.equal(true, data.response.is_template);
                assert.equal('object', typeof data.response.template);
                assert.equal('string', data.response.page_color);
                assert.equal(5, data.response.diagram_width);
                assert.equal(4, data.response.diagram_height);
                assert.equal(1, data.response.diagram_margin_size);
                assert.equal(false, data.response.is_blocked);
                assert.equal('string', data.response.visibility_layers_settings);
                assert.equal(3, data.response.auto_layout_state);
                assert.equal(9, data.response.map_coord_left);
                assert.equal(10, data.response.map_coord_top);
                assert.equal(2, data.response.map_coord_right);
                assert.equal(5, data.response.map_coord_bottom);
                assert.equal(4, data.response.map_units_type);
                assert.equal(3, data.response.map_source);
                assert.equal(true, data.response.is_display_grid);
                assert.equal(false, data.response.is_snap_to_grid);
                assert.equal(9, data.response.grid_spacing_x);
                assert.equal(8, data.response.grid_spacing_y);
                assert.equal(false, data.response.read_only_for_non_admins);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesDiagramSettings', 'rESTfulDiagramSettingsGetDiagramSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesMapCoordinatesId = 555;
    const nodesMapCoordinatesRESTfulNodeMapCoordinatesPutBodyParam = {
      x: 5,
      y: 3,
      units: 1,
      href: 'string'
    };
    describe('#rESTfulNodeMapCoordinatesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodeMapCoordinatesPut(nodesMapCoordinatesId, nodesMapCoordinatesRESTfulNodeMapCoordinatesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesMapCoordinates', 'rESTfulNodeMapCoordinatesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeMapCoordinatesGetMapCoordinates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulNodeMapCoordinatesGetMapCoordinates(nodesMapCoordinatesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.x);
                assert.equal(1, data.response.y);
                assert.equal(0, data.response.units);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesMapCoordinates', 'rESTfulNodeMapCoordinatesGetMapCoordinates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesVisNodesNodeId = 'fakedata';
    const nodesVisNodesRESTfulVisNodesPutBodyParam = {
      parent_diagram: {
        href: 'string'
      },
      displayed_fields: {
        href: 'string'
      },
      visual_override: {
        href: 'string'
      },
      is_reference_node: false,
      x: 2,
      y: 6,
      width: 8,
      height: 7,
      image_override: 'string',
      is_keep_aspect_ratio: false,
      can_move: false,
      can_resize: true,
      can_rotate: true,
      can_delete: true,
      is_hidden: false,
      z_order: 9,
      angle: 3,
      href: 'string'
    };
    describe('#rESTfulVisNodesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulVisNodesPut(nodesVisNodesRESTfulVisNodesPutBodyParam, nodesVisNodesNodeId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesVisNodes', 'rESTfulVisNodesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisNodesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulVisNodesGet(nodesVisNodesNodeId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesVisNodes', 'rESTfulVisNodesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisNodesGetOriginal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulVisNodesGetOriginal(nodesVisNodesNodeId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_diagram);
                assert.equal('object', typeof data.response.displayed_fields);
                assert.equal('object', typeof data.response.visual_override);
                assert.equal(true, data.response.is_reference_node);
                assert.equal(3, data.response.x);
                assert.equal(3, data.response.y);
                assert.equal(9, data.response.width);
                assert.equal(4, data.response.height);
                assert.equal('string', data.response.image_override);
                assert.equal(false, data.response.is_keep_aspect_ratio);
                assert.equal(false, data.response.can_move);
                assert.equal(true, data.response.can_resize);
                assert.equal(true, data.response.can_rotate);
                assert.equal(false, data.response.can_delete);
                assert.equal(true, data.response.is_hidden);
                assert.equal(3, data.response.z_order);
                assert.equal(6, data.response.angle);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesVisNodes', 'rESTfulVisNodesGetOriginal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisNodesGetReferences - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulVisNodesGetReferences(nodesVisNodesNodeId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesVisNodes', 'rESTfulVisNodesGetReferences', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesVisNodesId = 555;
    describe('#rESTfulVisNodesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulVisNodesGetById(nodesVisNodesNodeId, nodesVisNodesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_diagram);
                assert.equal('object', typeof data.response.displayed_fields);
                assert.equal('object', typeof data.response.visual_override);
                assert.equal(true, data.response.is_reference_node);
                assert.equal(3, data.response.x);
                assert.equal(10, data.response.y);
                assert.equal(1, data.response.width);
                assert.equal(8, data.response.height);
                assert.equal('string', data.response.image_override);
                assert.equal(false, data.response.is_keep_aspect_ratio);
                assert.equal(false, data.response.can_move);
                assert.equal(false, data.response.can_resize);
                assert.equal(false, data.response.can_rotate);
                assert.equal(true, data.response.can_delete);
                assert.equal(false, data.response.is_hidden);
                assert.equal(8, data.response.z_order);
                assert.equal(9, data.response.angle);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesVisNodes', 'rESTfulVisNodesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesRacksTypesContainerDesignsRESTfulRackContainerDesignsPostBodyParam = {
      parent_rack_type: {
        href: 'string'
      },
      name: 'string',
      rack_units_capacity: 9,
      rack_position_order: 1,
      physical_height: 7,
      physical_width: 10,
      x: 2,
      y: 4,
      width: 4,
      height: 10,
      href: 'string'
    };
    describe('#rESTfulRackContainerDesignsPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulRackContainerDesignsPost(nodeTypesRacksTypesContainerDesignsRESTfulRackContainerDesignsPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesRacksTypesContainerDesigns', 'rESTfulRackContainerDesignsPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesRacksTypesContainerDesignsRackTypeId = 555;
    const nodeTypesRacksTypesContainerDesignsPostWapiV1RackTypesRackTypeIdRackContainerDesignsBodyParam = {
      parent_rack_type: {
        href: 'string'
      },
      name: 'string',
      rack_units_capacity: 6,
      rack_position_order: 1,
      physical_height: 6,
      physical_width: 8,
      x: 5,
      y: 4,
      width: 6,
      height: 2,
      href: 'string'
    };
    describe('#postWapiV1RackTypesRackTypeIdRackContainerDesigns - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postWapiV1RackTypesRackTypeIdRackContainerDesigns(nodeTypesRacksTypesContainerDesignsRackTypeId, nodeTypesRacksTypesContainerDesignsPostWapiV1RackTypesRackTypeIdRackContainerDesignsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesRacksTypesContainerDesigns', 'postWapiV1RackTypesRackTypeIdRackContainerDesigns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesRacksTypesContainerDesignsRESTfulRackContainerDesignsPutBodyParam = {
      parent_rack_type: {
        href: 'string'
      },
      name: 'string',
      rack_units_capacity: 7,
      rack_position_order: 1,
      physical_height: 10,
      physical_width: 2,
      x: 3,
      y: 10,
      width: 7,
      height: 4,
      href: 'string'
    };
    describe('#rESTfulRackContainerDesignsPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulRackContainerDesignsPut(nodeTypesRacksTypesContainerDesignsRESTfulRackContainerDesignsPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesRacksTypesContainerDesigns', 'rESTfulRackContainerDesignsPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRackContainerDesignsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulRackContainerDesignsGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesRacksTypesContainerDesigns', 'rESTfulRackContainerDesignsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesRacksTypesContainerDesignsId = 555;
    describe('#rESTfulRackContainerDesignsGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulRackContainerDesignsGetById(nodeTypesRacksTypesContainerDesignsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_rack_type);
                assert.equal('string', data.response.name);
                assert.equal(2, data.response.rack_units_capacity);
                assert.equal(1, data.response.rack_position_order);
                assert.equal(8, data.response.physical_height);
                assert.equal(5, data.response.physical_width);
                assert.equal(7, data.response.x);
                assert.equal(3, data.response.y);
                assert.equal(3, data.response.width);
                assert.equal(7, data.response.height);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesRacksTypesContainerDesigns', 'rESTfulRackContainerDesignsGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1RackTypesRackTypeIdRackContainerDesigns - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1RackTypesRackTypeIdRackContainerDesigns(nodeTypesRacksTypesContainerDesignsRackTypeId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesRacksTypesContainerDesigns', 'getWapiV1RackTypesRackTypeIdRackContainerDesigns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesRacksTypesContainerDesignsPutWapiV1RackTypesRackTypeIdRackContainerDesignsIdBodyParam = {
      parent_rack_type: {
        href: 'string'
      },
      name: 'string',
      rack_units_capacity: 9,
      rack_position_order: 0,
      physical_height: 2,
      physical_width: 9,
      x: 9,
      y: 3,
      width: 10,
      height: 1,
      href: 'string'
    };
    describe('#putWapiV1RackTypesRackTypeIdRackContainerDesignsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1RackTypesRackTypeIdRackContainerDesignsId(nodeTypesRacksTypesContainerDesignsId, nodeTypesRacksTypesContainerDesignsRackTypeId, nodeTypesRacksTypesContainerDesignsPutWapiV1RackTypesRackTypeIdRackContainerDesignsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesRacksTypesContainerDesigns', 'putWapiV1RackTypesRackTypeIdRackContainerDesignsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1RackTypesRackTypeIdRackContainerDesignsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1RackTypesRackTypeIdRackContainerDesignsId(nodeTypesRacksTypesContainerDesignsRackTypeId, nodeTypesRacksTypesContainerDesignsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_rack_type);
                assert.equal('string', data.response.name);
                assert.equal(3, data.response.rack_units_capacity);
                assert.equal(0, data.response.rack_position_order);
                assert.equal(5, data.response.physical_height);
                assert.equal(6, data.response.physical_width);
                assert.equal(4, data.response.x);
                assert.equal(7, data.response.y);
                assert.equal(5, data.response.width);
                assert.equal(3, data.response.height);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesRacksTypesContainerDesigns', 'getWapiV1RackTypesRackTypeIdRackContainerDesignsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesRackTypesRESTfulRackTypesPostBodyParam = {};
    describe('#rESTfulRackTypesPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulRackTypesPost(nodeTypesRackTypesRESTfulRackTypesPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesRackTypes', 'rESTfulRackTypesPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesRackTypesRESTfulRackTypesPutBodyParam = {};
    describe('#rESTfulRackTypesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulRackTypesPut(nodeTypesRackTypesRESTfulRackTypesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesRackTypes', 'rESTfulRackTypesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRackTypesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulRackTypesGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesRackTypes', 'rESTfulRackTypesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodeTypesRackTypesId = 555;
    const nodeTypesRackTypesPutWapiV1RackTypesIdBodyParam = {};
    describe('#putWapiV1RackTypesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1RackTypesId(nodeTypesRackTypesId, nodeTypesRackTypesPutWapiV1RackTypesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesRackTypes', 'putWapiV1RackTypesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRackTypesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulRackTypesGetById(nodeTypesRackTypesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.base_node_type);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.background);
                assert.equal(1, data.response.rack_units_capacity);
                assert.equal(1, data.response.power);
                assert.equal(3, data.response.max_weight);
                assert.equal(true, data.response.is_display_rack_lines);
                assert.equal(2, data.response.in_stock);
                assert.equal('object', typeof data.response.vendor);
                assert.equal('object', typeof data.response.rack_container_designs);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesRackTypes', 'rESTfulRackTypesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesRacksRESTfulRacksPostBodyParam = {};
    describe('#rESTfulRacksPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulRacksPost(nodesRacksRESTfulRacksPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesRacks', 'rESTfulRacksPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRacksGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulRacksGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesRacks', 'rESTfulRacksGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesRacksId = 555;
    const nodesRacksRESTfulRacksPutBodyParam = {};
    describe('#rESTfulRacksPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulRacksPut(nodesRacksId, nodesRacksRESTfulRacksPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesRacks', 'rESTfulRacksPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRacksGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulRacksGetById(nodesRacksId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.base_node);
                assert.equal('object', typeof data.response.rack_type);
                assert.equal('string', data.response.name);
                assert.equal(4, data.response.available_rack_units);
                assert.equal(8, data.response.power);
                assert.equal(1, data.response.power_used);
                assert.equal(6, data.response.max_weight);
                assert.equal(1, data.response.weight_used);
                assert.equal('object', typeof data.response.rack_containers);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesRacks', 'rESTfulRacksGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesRacksRackId = 555;
    describe('#rESTfulRackContainersGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulRackContainersGet(nodesRacksRackId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesRacks', 'rESTfulRackContainersGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRackContainersGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulRackContainersGetById(nodesRacksRackId, nodesRacksId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_rack);
                assert.equal('object', typeof data.response.rack_container_design);
                assert.equal('object', typeof data.response.mounted_devices);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesRacks', 'rESTfulRackContainersGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRolesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulRolesGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserRoles', 'rESTfulRolesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userRolesId = 555;
    describe('#rESTfulRolesGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulRolesGetById(userRolesId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal(-1, data.response.permissions);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserRoles', 'rESTfulRolesGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSearchGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulSearchGet(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Search', 'rESTfulSearchGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workOrdersTasksWorkOrderId = 'fakedata';
    const workOrdersTasksRESTfulWorkOrderTasksPostBodyParam = {
      parent_work_order: {
        href: 'string'
      },
      performer: {
        href: 'string'
      },
      object: {
        href: 'string'
      },
      name: 'string',
      due_date: 'string',
      comments: 'string',
      type: 2,
      status: 2,
      is_performer_notified: true,
      is_performer_notified_about_overdue: false,
      href: 'string'
    };
    describe('#rESTfulWorkOrderTasksPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulWorkOrderTasksPost(workOrdersTasksRESTfulWorkOrderTasksPostBodyParam, workOrdersTasksWorkOrderId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkOrdersTasks', 'rESTfulWorkOrderTasksPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulWorkOrderTasksGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulWorkOrderTasksGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkOrdersTasks', 'rESTfulWorkOrderTasksGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workOrdersTasksRESTfulWorkOrderTasksPutBodyParam = {
      parent_work_order: {
        href: 'string'
      },
      performer: {
        href: 'string'
      },
      object: {
        href: 'string'
      },
      name: 'string',
      due_date: 'string',
      comments: 'string',
      type: 0,
      status: 4,
      is_performer_notified: false,
      is_performer_notified_about_overdue: false,
      href: 'string'
    };
    describe('#rESTfulWorkOrderTasksPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulWorkOrderTasksPut(workOrdersTasksRESTfulWorkOrderTasksPutBodyParam, workOrdersTasksWorkOrderId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkOrdersTasks', 'rESTfulWorkOrderTasksPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1WorkOrdersWorkOrderIdTasks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWapiV1WorkOrdersWorkOrderIdTasks(workOrdersTasksWorkOrderId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkOrdersTasks', 'getWapiV1WorkOrdersWorkOrderIdTasks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workOrdersTasksId = 555;
    describe('#rESTfulWorkOrderTasksGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulWorkOrderTasksGetById(workOrdersTasksWorkOrderId, workOrdersTasksId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_work_order);
                assert.equal('object', typeof data.response.performer);
                assert.equal('object', typeof data.response.object);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.due_date);
                assert.equal('string', data.response.comments);
                assert.equal(2, data.response.type);
                assert.equal(4, data.response.status);
                assert.equal(true, data.response.is_performer_notified);
                assert.equal(false, data.response.is_performer_notified_about_overdue);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkOrdersTasks', 'rESTfulWorkOrderTasksGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersRESTfulUsersPostBodyParam = {
      is_override_ad_group: true,
      name: 'string',
      is_locked: true,
      account_type: 1,
      email: 'string',
      description: 'string',
      comments: 'string',
      group: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#rESTfulUsersPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulUsersPost(usersRESTfulUsersPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'rESTfulUsersPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersRESTfulUsersPutBodyParam = {
      is_override_ad_group: true,
      name: 'string',
      is_locked: false,
      account_type: 0,
      email: 'string',
      description: 'string',
      comments: 'string',
      group: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#rESTfulUsersPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulUsersPut(usersRESTfulUsersPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'rESTfulUsersPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulUsersGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulUsersGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'rESTfulUsersGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersId = 555;
    const usersPutWapiV1UsersIdBodyParam = {
      is_override_ad_group: false,
      name: 'string',
      is_locked: false,
      account_type: 0,
      email: 'string',
      description: 'string',
      comments: 'string',
      group: {
        href: 'string'
      },
      href: 'string'
    };
    describe('#putWapiV1UsersId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1UsersId(usersId, usersPutWapiV1UsersIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'putWapiV1UsersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulUsersGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulUsersGetById(usersId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.is_override_ad_group);
                assert.equal('string', data.response.name);
                assert.equal(true, data.response.is_locked);
                assert.equal(1, data.response.account_type);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.comments);
                assert.equal('object', typeof data.response.group);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'rESTfulUsersGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersRESTfulUsersSetPasswordBodyParam = {};
    describe('#rESTfulUsersSetPassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulUsersSetPassword(usersId, usersRESTfulUsersSetPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'rESTfulUsersSetPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorsRESTfulVendorsPostBodyParam = {
      name: 'string',
      href: 'string'
    };
    describe('#rESTfulVendorsPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulVendorsPost(vendorsRESTfulVendorsPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'rESTfulVendorsPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorsRESTfulVendorsPutBodyParam = {
      name: 'string',
      href: 'string'
    };
    describe('#rESTfulVendorsPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulVendorsPut(vendorsRESTfulVendorsPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'rESTfulVendorsPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVendorsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulVendorsGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'rESTfulVendorsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorsId = 555;
    const vendorsPutWapiV1VendorsIdBodyParam = {
      name: 'string',
      href: 'string'
    };
    describe('#putWapiV1VendorsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWapiV1VendorsId(vendorsId, vendorsPutWapiV1VendorsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'putWapiV1VendorsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVendorsGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulVendorsGetById(vendorsId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'rESTfulVendorsGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linksVisLinksDisplayedFieldsVisLinkId = 'fakedata';
    const linksVisLinksDisplayedFieldsRESTfulVisLinkDisplayedFieldsPostBodyParam = {
      parent_vis_link: {
        href: 'string'
      },
      parent_link_property_value: {
        href: 'string'
      },
      offset_x: 1,
      offset_y: 3,
      anchor: 6,
      upright_alignment: false,
      font_size: 3,
      color: 'string',
      background_color: 'string',
      justification: 5,
      can_move: false,
      angle: 3,
      is_displayed: true,
      is_bold: true,
      is_italic: false,
      is_underline: true,
      font_family: 'string',
      align: 7,
      href: 'string'
    };
    describe('#rESTfulVisLinkDisplayedFieldsPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulVisLinkDisplayedFieldsPost(linksVisLinksDisplayedFieldsRESTfulVisLinkDisplayedFieldsPostBodyParam, linksVisLinksDisplayedFieldsVisLinkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinksVisLinksDisplayedFields', 'rESTfulVisLinkDisplayedFieldsPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linksVisLinksDisplayedFieldsRESTfulVisLinkDisplayedFieldsPutBodyParam = {
      parent_vis_link: {
        href: 'string'
      },
      parent_link_property_value: {
        href: 'string'
      },
      offset_x: 2,
      offset_y: 5,
      anchor: 2,
      upright_alignment: true,
      font_size: 8,
      color: 'string',
      background_color: 'string',
      justification: 10,
      can_move: true,
      angle: 3,
      is_displayed: false,
      is_bold: true,
      is_italic: true,
      is_underline: true,
      font_family: 'string',
      align: 2,
      href: 'string'
    };
    describe('#rESTfulVisLinkDisplayedFieldsPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulVisLinkDisplayedFieldsPut(linksVisLinksDisplayedFieldsRESTfulVisLinkDisplayedFieldsPutBodyParam, linksVisLinksDisplayedFieldsVisLinkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinksVisLinksDisplayedFields', 'rESTfulVisLinkDisplayedFieldsPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisLinkDisplayedFieldsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulVisLinkDisplayedFieldsGet(linksVisLinksDisplayedFieldsVisLinkId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinksVisLinksDisplayedFields', 'rESTfulVisLinkDisplayedFieldsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linksVisLinksDisplayedFieldsLinkPropertyValueId = 555;
    describe('#rESTfulVisLinkDisplayedFieldsGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulVisLinkDisplayedFieldsGetById(linksVisLinksDisplayedFieldsLinkPropertyValueId, linksVisLinksDisplayedFieldsVisLinkId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_vis_link);
                assert.equal('object', typeof data.response.parent_link_property_value);
                assert.equal(6, data.response.offset_x);
                assert.equal(2, data.response.offset_y);
                assert.equal(6, data.response.anchor);
                assert.equal(false, data.response.upright_alignment);
                assert.equal(4, data.response.font_size);
                assert.equal('string', data.response.color);
                assert.equal('string', data.response.background_color);
                assert.equal(9, data.response.justification);
                assert.equal(true, data.response.can_move);
                assert.equal(2, data.response.angle);
                assert.equal(true, data.response.is_displayed);
                assert.equal(true, data.response.is_bold);
                assert.equal(true, data.response.is_italic);
                assert.equal(true, data.response.is_underline);
                assert.equal('string', data.response.font_family);
                assert.equal(9, data.response.align);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinksVisLinksDisplayedFields', 'rESTfulVisLinkDisplayedFieldsGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesVisNodesDisplayedFieldsVisNodeId = 'fakedata';
    const nodesVisNodesDisplayedFieldsRESTfulVisNodeDisplayedFieldsPostBodyParam = {
      parent_vis_node: {
        href: 'string'
      },
      parent_node_property_value: {
        href: 'string'
      },
      offset_x: 3,
      offset_y: 6,
      font_size: 9,
      color: 'string',
      background_color: 'string',
      justification: 5,
      can_move: true,
      angle: 1,
      is_displayed: false,
      is_bold: false,
      is_italic: true,
      is_underline: true,
      font_family: 'string',
      align: 5,
      href: 'string'
    };
    describe('#rESTfulVisNodeDisplayedFieldsPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulVisNodeDisplayedFieldsPost(nodesVisNodesDisplayedFieldsRESTfulVisNodeDisplayedFieldsPostBodyParam, nodesVisNodesDisplayedFieldsVisNodeId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesVisNodesDisplayedFields', 'rESTfulVisNodeDisplayedFieldsPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesVisNodesDisplayedFieldsRESTfulVisNodeDisplayedFieldsPutBodyParam = {
      parent_vis_node: {
        href: 'string'
      },
      parent_node_property_value: {
        href: 'string'
      },
      offset_x: 6,
      offset_y: 5,
      font_size: 3,
      color: 'string',
      background_color: 'string',
      justification: 7,
      can_move: true,
      angle: 7,
      is_displayed: false,
      is_bold: true,
      is_italic: false,
      is_underline: true,
      font_family: 'string',
      align: 3,
      href: 'string'
    };
    describe('#rESTfulVisNodeDisplayedFieldsPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulVisNodeDisplayedFieldsPut(nodesVisNodesDisplayedFieldsRESTfulVisNodeDisplayedFieldsPutBodyParam, nodesVisNodesDisplayedFieldsVisNodeId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesVisNodesDisplayedFields', 'rESTfulVisNodeDisplayedFieldsPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisNodeDisplayedFieldsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulVisNodeDisplayedFieldsGet(nodesVisNodesDisplayedFieldsVisNodeId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesVisNodesDisplayedFields', 'rESTfulVisNodeDisplayedFieldsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesVisNodesDisplayedFieldsNodePropertyValueId = 555;
    describe('#rESTfulVisNodeDisplayedFieldsGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulVisNodeDisplayedFieldsGetById(nodesVisNodesDisplayedFieldsNodePropertyValueId, nodesVisNodesDisplayedFieldsVisNodeId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.parent_vis_node);
                assert.equal('object', typeof data.response.parent_node_property_value);
                assert.equal(1, data.response.offset_x);
                assert.equal(9, data.response.offset_y);
                assert.equal(7, data.response.font_size);
                assert.equal('string', data.response.color);
                assert.equal('string', data.response.background_color);
                assert.equal(5, data.response.justification);
                assert.equal(false, data.response.can_move);
                assert.equal(6, data.response.angle);
                assert.equal(false, data.response.is_displayed);
                assert.equal(false, data.response.is_bold);
                assert.equal(false, data.response.is_italic);
                assert.equal(false, data.response.is_underline);
                assert.equal('string', data.response.font_family);
                assert.equal(4, data.response.align);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesVisNodesDisplayedFields', 'rESTfulVisNodeDisplayedFieldsGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workOrdersRESTfulWorkOrdersPostBodyParam = {
      owner: {
        href: 'string'
      },
      name: 'string',
      due_date: 'string',
      comments: 'string',
      is_archived: false,
      is_owner_notified: false,
      is_owner_notified_about_overdue: true,
      href: 'string'
    };
    describe('#rESTfulWorkOrdersPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulWorkOrdersPost(workOrdersRESTfulWorkOrdersPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkOrders', 'rESTfulWorkOrdersPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workOrdersRESTfulWorkOrdersPutBodyParam = {
      owner: {
        href: 'string'
      },
      name: 'string',
      due_date: 'string',
      comments: 'string',
      is_archived: false,
      is_owner_notified: false,
      is_owner_notified_about_overdue: true,
      href: 'string'
    };
    describe('#rESTfulWorkOrdersPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulWorkOrdersPut(workOrdersRESTfulWorkOrdersPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkOrders', 'rESTfulWorkOrdersPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulWorkOrdersGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulWorkOrdersGet(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkOrders', 'rESTfulWorkOrdersGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workOrdersId = 555;
    describe('#rESTfulWorkOrdersGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rESTfulWorkOrdersGetById(workOrdersId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.owner);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.due_date);
                assert.equal('string', data.response.comments);
                assert.equal(true, data.response.is_archived);
                assert.equal(true, data.response.is_owner_notified);
                assert.equal(false, data.response.is_owner_notified_about_overdue);
                assert.equal('string', data.response.href);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkOrders', 'rESTfulWorkOrdersGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCardsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulCardsDelete(nodesCardsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesCards', 'rESTfulCardsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinksDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulLinksDelete(linksId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Links', 'rESTfulLinksDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulPortRepresentationsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulPortRepresentationsDelete(nodeTypesPortRepresentationsTypeGroup, nodeTypesPortRepresentationsTypeId, nodeTypesPortRepresentationsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPortRepresentations', 'rESTfulPortRepresentationsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotRepresentationsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulSlotRepresentationsDelete(nodeTypesSlotRepresentationsTypeGroup, nodeTypesSlotRepresentationsTypeId, nodeTypesSlotRepresentationsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentations', 'rESTfulSlotRepresentationsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotMappingDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulSlotMappingDelete(nodeTypesSlotRepresentationsSlotMappingsTypeGroup, nodeTypesSlotRepresentationsSlotMappingsTypeId, nodeTypesSlotRepresentationsSlotMappingsSlotRepresentationId, nodeTypesSlotRepresentationsSlotMappingsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesSlotRepresentationsSlotMappings', 'rESTfulSlotMappingDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCardTypesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulCardTypesDelete(nodeTypesCardTypesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesCardTypes', 'rESTfulCardTypesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCircuitPropertiesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulCircuitPropertiesDelete(circuitPropertiesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CircuitProperties', 'rESTfulCircuitPropertiesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCircuitsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulCircuitsDelete(circuitsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'rESTfulCircuitsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDeviceTypesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulDeviceTypesDelete(nodeTypesDeviceTypesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesDeviceTypes', 'rESTfulDeviceTypesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDevicesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulDevicesDelete(nodesDevicesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesDevices', 'rESTfulDevicesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulFreeTextDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulFreeTextDelete(freeTextId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FreeText', 'rESTfulFreeTextDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulGroupsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulGroupsDelete(userGroupsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserGroups', 'rESTfulGroupsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkCategoriesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulLinkCategoriesDelete(linkCategoriesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkCategories', 'rESTfulLinkCategoriesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkOverridesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulLinkOverridesDelete(linkTypesPropertiesOverridesId, linkTypesPropertiesOverridesLinkTypeId, linkTypesPropertiesOverridesLinkTypePropertyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesPropertiesOverrides', 'rESTfulLinkOverridesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkTypePropertiesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulLinkTypePropertiesDelete(linkTypesPropertiesId, linkTypesPropertiesLinkTypeId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypesProperties', 'rESTfulLinkTypePropertiesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkTypesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulLinkTypesDelete(linkTypesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkTypes', 'rESTfulLinkTypesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeCategoriesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodeCategoriesDelete(nodeCategoriesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeCategories', 'rESTfulNodeCategoriesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeOverridesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodeOverridesDelete(nodeTypesPropertiesOverridesId, nodeTypesPropertiesOverridesNodeTypeId, nodeTypesPropertiesOverridesNodeTypePropertyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesPropertiesOverrides', 'rESTfulNodeOverridesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeTypePropertiesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodeTypePropertiesDelete(nodeTypesPropertiesId, nodeTypesPropertiesNodeTypeId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesProperties', 'rESTfulNodeTypePropertiesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeTypesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodeTypesDelete(nodeTypesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypes', 'rESTfulNodeTypesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulNodesDelete(nodesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'rESTfulNodesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRackContainerDesignsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulRackContainerDesignsDelete(nodeTypesRacksTypesContainerDesignsId, nodeTypesRacksTypesContainerDesignsRackTypeId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesRacksTypesContainerDesigns', 'rESTfulRackContainerDesignsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRackTypesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulRackTypesDelete(nodeTypesRackTypesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodeTypesRackTypes', 'rESTfulRackTypesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRacksDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulRacksDelete(nodesRacksId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NodesRacks', 'rESTfulRacksDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulWorkOrderTasksDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulWorkOrderTasksDelete(workOrdersTasksId, workOrdersTasksWorkOrderId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkOrdersTasks', 'rESTfulWorkOrderTasksDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulUsersDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulUsersDelete(usersId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'rESTfulUsersDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVendorsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulVendorsDelete(vendorsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendors', 'rESTfulVendorsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulWorkOrdersDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rESTfulWorkOrdersDelete(workOrdersId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netterrain-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkOrders', 'rESTfulWorkOrdersDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
