/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-netterrain',
      type: 'Netterrain',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Netterrain = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Netterrain Adapter Test', () => {
  describe('Netterrain Class Tests', () => {
    const a = new Netterrain(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('netterrain'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('netterrain'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Netterrain', pronghornDotJson.export);
          assert.equal('Netterrain', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-netterrain', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('netterrain'));
          assert.equal('Netterrain', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-netterrain', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-netterrain', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#rESTfulCardsGet - errors', () => {
      it('should have a rESTfulCardsGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCardsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentTypeGroup', (done) => {
        try {
          a.rESTfulCardsGet(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'parentTypeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCardsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing parentId', (done) => {
        try {
          a.rESTfulCardsGet('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'parentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCardsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1Cards - errors', () => {
      it('should have a getWapiV1Cards function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1Cards === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCardsGetById - errors', () => {
      it('should have a rESTfulCardsGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCardsGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulCardsGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCardsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCardsPut - errors', () => {
      it('should have a rESTfulCardsPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCardsPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulCardsPut(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCardsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulCardsPut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCardsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCardsDelete - errors', () => {
      it('should have a rESTfulCardsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCardsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulCardsDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCardsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulPortsGet - errors', () => {
      it('should have a rESTfulPortsGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulPortsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulPortsGet(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.rESTfulPortsGet('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulPortsGetById - errors', () => {
      it('should have a rESTfulPortsGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulPortsGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulPortsGetById(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.rESTfulPortsGetById('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulPortsGetById('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1Ports - errors', () => {
      it('should have a getWapiV1Ports function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1Ports === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinksGet - errors', () => {
      it('should have a rESTfulLinksGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinksGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulLinksGet(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinksGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.rESTfulLinksGet('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinksGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulLinksGet('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinksGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1Links - errors', () => {
      it('should have a getWapiV1Links function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1Links === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinksPut - errors', () => {
      it('should have a rESTfulLinksPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinksPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulLinksPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinksPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinksPost - errors', () => {
      it('should have a rESTfulLinksPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinksPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulLinksPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinksPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinksGetById - errors', () => {
      it('should have a rESTfulLinksGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinksGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulLinksGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinksGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1LinksId - errors', () => {
      it('should have a putWapiV1LinksId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1LinksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1LinksId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1LinksId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinksDelete - errors', () => {
      it('should have a rESTfulLinksDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinksDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulLinksDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinksDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotsGet - errors', () => {
      it('should have a rESTfulSlotsGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulSlotsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulSlotsGet(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.rESTfulSlotsGet('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotsGetById - errors', () => {
      it('should have a rESTfulSlotsGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulSlotsGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulSlotsGetById(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.rESTfulSlotsGetById('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulSlotsGetById('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotsPost - errors', () => {
      it('should have a rESTfulSlotsPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulSlotsPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulSlotsPost(null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.rESTfulSlotsPost('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulSlotsPost('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulSlotsPost('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1Slots - errors', () => {
      it('should have a getWapiV1Slots function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1Slots === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulPortRepresentationsGet - errors', () => {
      it('should have a rESTfulPortRepresentationsGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulPortRepresentationsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulPortRepresentationsGet(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortRepresentationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.rESTfulPortRepresentationsGet('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortRepresentationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulPortRepresentationsPost - errors', () => {
      it('should have a rESTfulPortRepresentationsPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulPortRepresentationsPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulPortRepresentationsPost(null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortRepresentationsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.rESTfulPortRepresentationsPost('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortRepresentationsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulPortRepresentationsPost('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortRepresentationsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulPortRepresentationsGetById - errors', () => {
      it('should have a rESTfulPortRepresentationsGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulPortRepresentationsGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulPortRepresentationsGetById(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortRepresentationsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.rESTfulPortRepresentationsGetById('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortRepresentationsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulPortRepresentationsGetById('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortRepresentationsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulPortRepresentationsPut - errors', () => {
      it('should have a rESTfulPortRepresentationsPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulPortRepresentationsPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulPortRepresentationsPut(null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortRepresentationsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.rESTfulPortRepresentationsPut('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortRepresentationsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulPortRepresentationsPut('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortRepresentationsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulPortRepresentationsPut('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortRepresentationsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulPortRepresentationsDelete - errors', () => {
      it('should have a rESTfulPortRepresentationsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulPortRepresentationsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulPortRepresentationsDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortRepresentationsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.rESTfulPortRepresentationsDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortRepresentationsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulPortRepresentationsDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortRepresentationsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1PortRepresentations - errors', () => {
      it('should have a getWapiV1PortRepresentations function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1PortRepresentations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1PortRepresentations - errors', () => {
      it('should have a putWapiV1PortRepresentations function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1PortRepresentations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1PortRepresentations(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1PortRepresentations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWapiV1PortRepresentations - errors', () => {
      it('should have a postWapiV1PortRepresentations function', (done) => {
        try {
          assert.equal(true, typeof a.postWapiV1PortRepresentations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postWapiV1PortRepresentations(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-postWapiV1PortRepresentations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1PortRepresentationsId - errors', () => {
      it('should have a getWapiV1PortRepresentationsId function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1PortRepresentationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getWapiV1PortRepresentationsId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1PortRepresentationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulPortFieldsGet - errors', () => {
      it('should have a rESTfulPortFieldsGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulPortFieldsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portRepresentationId', (done) => {
        try {
          a.rESTfulPortFieldsGet(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'portRepresentationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortFieldsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulPortFieldsGet('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortFieldsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.rESTfulPortFieldsGet('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortFieldsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulPortFieldsGetById - errors', () => {
      it('should have a rESTfulPortFieldsGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulPortFieldsGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulPortFieldsGetById(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortFieldsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.rESTfulPortFieldsGetById('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortFieldsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portRepresentationId', (done) => {
        try {
          a.rESTfulPortFieldsGetById('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'portRepresentationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortFieldsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulPortFieldsGetById('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulPortFieldsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1PortFields - errors', () => {
      it('should have a getWapiV1PortFields function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1PortFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1PortFieldsId - errors', () => {
      it('should have a getWapiV1PortFieldsId function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1PortFieldsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getWapiV1PortFieldsId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1PortFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotRepresentationsGet - errors', () => {
      it('should have a rESTfulSlotRepresentationsGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulSlotRepresentationsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulSlotRepresentationsGet(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotRepresentationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.rESTfulSlotRepresentationsGet('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotRepresentationsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotRepresentationsPost - errors', () => {
      it('should have a rESTfulSlotRepresentationsPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulSlotRepresentationsPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulSlotRepresentationsPost(null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotRepresentationsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.rESTfulSlotRepresentationsPost('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotRepresentationsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulSlotRepresentationsPost('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotRepresentationsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotRepresentationsGetById - errors', () => {
      it('should have a rESTfulSlotRepresentationsGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulSlotRepresentationsGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulSlotRepresentationsGetById(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotRepresentationsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.rESTfulSlotRepresentationsGetById('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotRepresentationsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulSlotRepresentationsGetById('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotRepresentationsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotRepresentationsPut - errors', () => {
      it('should have a rESTfulSlotRepresentationsPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulSlotRepresentationsPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulSlotRepresentationsPut(null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotRepresentationsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.rESTfulSlotRepresentationsPut('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotRepresentationsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulSlotRepresentationsPut('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotRepresentationsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulSlotRepresentationsPut('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotRepresentationsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotRepresentationsDelete - errors', () => {
      it('should have a rESTfulSlotRepresentationsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulSlotRepresentationsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulSlotRepresentationsDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotRepresentationsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.rESTfulSlotRepresentationsDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotRepresentationsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulSlotRepresentationsDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotRepresentationsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1SlotRepresentations - errors', () => {
      it('should have a getWapiV1SlotRepresentations function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1SlotRepresentations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1SlotRepresentations - errors', () => {
      it('should have a putWapiV1SlotRepresentations function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1SlotRepresentations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1SlotRepresentations(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1SlotRepresentations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWapiV1SlotRepresentations - errors', () => {
      it('should have a postWapiV1SlotRepresentations function', (done) => {
        try {
          assert.equal(true, typeof a.postWapiV1SlotRepresentations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postWapiV1SlotRepresentations(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-postWapiV1SlotRepresentations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1SlotRepresentationsId - errors', () => {
      it('should have a getWapiV1SlotRepresentationsId function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1SlotRepresentationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getWapiV1SlotRepresentationsId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1SlotRepresentationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotFieldsGet - errors', () => {
      it('should have a rESTfulSlotFieldsGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulSlotFieldsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing slotRepresentationId', (done) => {
        try {
          a.rESTfulSlotFieldsGet(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'slotRepresentationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotFieldsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulSlotFieldsGet('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotFieldsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.rESTfulSlotFieldsGet('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotFieldsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotFieldsGetById - errors', () => {
      it('should have a rESTfulSlotFieldsGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulSlotFieldsGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulSlotFieldsGetById(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotFieldsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.rESTfulSlotFieldsGetById('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotFieldsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing slotRepresentationId', (done) => {
        try {
          a.rESTfulSlotFieldsGetById('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'slotRepresentationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotFieldsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulSlotFieldsGetById('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotFieldsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1SlotFields - errors', () => {
      it('should have a getWapiV1SlotFields function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1SlotFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1SlotFieldsId - errors', () => {
      it('should have a getWapiV1SlotFieldsId function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1SlotFieldsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getWapiV1SlotFieldsId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1SlotFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotMappingGet - errors', () => {
      it('should have a rESTfulSlotMappingGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulSlotMappingGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing slotRepresentationId', (done) => {
        try {
          a.rESTfulSlotMappingGet(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'slotRepresentationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotMappingGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulSlotMappingGet('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotMappingGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.rESTfulSlotMappingGet('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotMappingGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotMappingPost - errors', () => {
      it('should have a rESTfulSlotMappingPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulSlotMappingPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulSlotMappingPost(null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotMappingPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.rESTfulSlotMappingPost('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotMappingPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing slotRepresentationId', (done) => {
        try {
          a.rESTfulSlotMappingPost('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'slotRepresentationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotMappingPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulSlotMappingPost('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotMappingPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotMappingGetById - errors', () => {
      it('should have a rESTfulSlotMappingGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulSlotMappingGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulSlotMappingGetById(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotMappingGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.rESTfulSlotMappingGetById('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotMappingGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing slotRepresentationId', (done) => {
        try {
          a.rESTfulSlotMappingGetById('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'slotRepresentationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotMappingGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulSlotMappingGetById('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotMappingGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSlotMappingDelete - errors', () => {
      it('should have a rESTfulSlotMappingDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulSlotMappingDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeGroup', (done) => {
        try {
          a.rESTfulSlotMappingDelete(null, null, null, null, (data, error) => {
            try {
              const displayE = 'typeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotMappingDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing typeId', (done) => {
        try {
          a.rESTfulSlotMappingDelete('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'typeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotMappingDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing slotRepresentationId', (done) => {
        try {
          a.rESTfulSlotMappingDelete('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'slotRepresentationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotMappingDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulSlotMappingDelete('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulSlotMappingDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1SlotMapping - errors', () => {
      it('should have a getWapiV1SlotMapping function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1SlotMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWapiV1SlotMapping - errors', () => {
      it('should have a postWapiV1SlotMapping function', (done) => {
        try {
          assert.equal(true, typeof a.postWapiV1SlotMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postWapiV1SlotMapping(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-postWapiV1SlotMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1SlotMappingId - errors', () => {
      it('should have a getWapiV1SlotMappingId function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1SlotMappingId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getWapiV1SlotMappingId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1SlotMappingId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCardTypesGet - errors', () => {
      it('should have a rESTfulCardTypesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCardTypesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCardTypesPut - errors', () => {
      it('should have a rESTfulCardTypesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCardTypesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulCardTypesPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCardTypesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCardTypesPost - errors', () => {
      it('should have a rESTfulCardTypesPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCardTypesPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulCardTypesPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCardTypesPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCardTypesGetById - errors', () => {
      it('should have a rESTfulCardTypesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCardTypesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulCardTypesGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCardTypesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1CardTypesId - errors', () => {
      it('should have a putWapiV1CardTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1CardTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1CardTypesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1CardTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1CardTypesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1CardTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCardTypesDelete - errors', () => {
      it('should have a rESTfulCardTypesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCardTypesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulCardTypesDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCardTypesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCircuitPropertiesGet - errors', () => {
      it('should have a rESTfulCircuitPropertiesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCircuitPropertiesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCircuitPropertiesPut - errors', () => {
      it('should have a rESTfulCircuitPropertiesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCircuitPropertiesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing property', (done) => {
        try {
          a.rESTfulCircuitPropertiesPut(null, (data, error) => {
            try {
              const displayE = 'property is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCircuitPropertiesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCircuitPropertiesPost - errors', () => {
      it('should have a rESTfulCircuitPropertiesPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCircuitPropertiesPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitProperty', (done) => {
        try {
          a.rESTfulCircuitPropertiesPost(null, (data, error) => {
            try {
              const displayE = 'circuitProperty is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCircuitPropertiesPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCircuitPropertiesGetById - errors', () => {
      it('should have a rESTfulCircuitPropertiesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCircuitPropertiesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulCircuitPropertiesGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCircuitPropertiesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCircuitPropertiesDelete - errors', () => {
      it('should have a rESTfulCircuitPropertiesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCircuitPropertiesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulCircuitPropertiesDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCircuitPropertiesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCircuitPropertyValuesGet - errors', () => {
      it('should have a rESTfulCircuitPropertyValuesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCircuitPropertyValuesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1CircuitsCircuitIdCircuitPropertyValues - errors', () => {
      it('should have a getWapiV1CircuitsCircuitIdCircuitPropertyValues function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1CircuitsCircuitIdCircuitPropertyValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitId', (done) => {
        try {
          a.getWapiV1CircuitsCircuitIdCircuitPropertyValues(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'circuitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1CircuitsCircuitIdCircuitPropertyValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCircuitPropertyValuesPut - errors', () => {
      it('should have a rESTfulCircuitPropertyValuesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCircuitPropertyValuesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitId', (done) => {
        try {
          a.rESTfulCircuitPropertyValuesPut(null, null, (data, error) => {
            try {
              const displayE = 'circuitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCircuitPropertyValuesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitPropertyValue', (done) => {
        try {
          a.rESTfulCircuitPropertyValuesPut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'circuitPropertyValue is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCircuitPropertyValuesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCircuitPropertyValuesGetById - errors', () => {
      it('should have a rESTfulCircuitPropertyValuesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCircuitPropertyValuesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitId', (done) => {
        try {
          a.rESTfulCircuitPropertyValuesGetById(null, null, null, null, (data, error) => {
            try {
              const displayE = 'circuitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCircuitPropertyValuesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulCircuitPropertyValuesGetById('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCircuitPropertyValuesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCircuitPropertyValuesInitAbsentValues - errors', () => {
      it('should have a rESTfulCircuitPropertyValuesInitAbsentValues function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCircuitPropertyValuesInitAbsentValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing circuitId', (done) => {
        try {
          a.rESTfulCircuitPropertyValuesInitAbsentValues(null, (data, error) => {
            try {
              const displayE = 'circuitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCircuitPropertyValuesInitAbsentValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCircuitsGet - errors', () => {
      it('should have a rESTfulCircuitsGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCircuitsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCircuitsPost - errors', () => {
      it('should have a rESTfulCircuitsPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCircuitsPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addCircuitData', (done) => {
        try {
          a.rESTfulCircuitsPost(null, (data, error) => {
            try {
              const displayE = 'addCircuitData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCircuitsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCircuitsGetById - errors', () => {
      it('should have a rESTfulCircuitsGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCircuitsGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulCircuitsGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCircuitsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulCircuitsDelete - errors', () => {
      it('should have a rESTfulCircuitsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulCircuitsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulCircuitsDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulCircuitsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDeviceTypesGet - errors', () => {
      it('should have a rESTfulDeviceTypesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulDeviceTypesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDeviceTypesPut - errors', () => {
      it('should have a rESTfulDeviceTypesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulDeviceTypesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulDeviceTypesPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulDeviceTypesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDeviceTypesPost - errors', () => {
      it('should have a rESTfulDeviceTypesPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulDeviceTypesPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulDeviceTypesPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulDeviceTypesPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDeviceTypesGetById - errors', () => {
      it('should have a rESTfulDeviceTypesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulDeviceTypesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulDeviceTypesGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulDeviceTypesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1DeviceTypesId - errors', () => {
      it('should have a putWapiV1DeviceTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1DeviceTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1DeviceTypesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1DeviceTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1DeviceTypesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1DeviceTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDeviceTypesDelete - errors', () => {
      it('should have a rESTfulDeviceTypesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulDeviceTypesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulDeviceTypesDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulDeviceTypesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDevicesGet - errors', () => {
      it('should have a rESTfulDevicesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulDevicesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDevicesPost - errors', () => {
      it('should have a rESTfulDevicesPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulDevicesPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulDevicesPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulDevicesPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDevicesGetById - errors', () => {
      it('should have a rESTfulDevicesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulDevicesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulDevicesGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulDevicesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDevicesPut - errors', () => {
      it('should have a rESTfulDevicesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulDevicesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulDevicesPut(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulDevicesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulDevicesPut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulDevicesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDevicesDelete - errors', () => {
      it('should have a rESTfulDevicesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulDevicesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulDevicesDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulDevicesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1RacksRackIdContainersContainerIdMountedDevices - errors', () => {
      it('should have a getWapiV1RacksRackIdContainersContainerIdMountedDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1RacksRackIdContainersContainerIdMountedDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rackId', (done) => {
        try {
          a.getWapiV1RacksRackIdContainersContainerIdMountedDevices(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'rackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1RacksRackIdContainersContainerIdMountedDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerId', (done) => {
        try {
          a.getWapiV1RacksRackIdContainersContainerIdMountedDevices('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'containerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1RacksRackIdContainersContainerIdMountedDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDevicesMountDevice - errors', () => {
      it('should have a rESTfulDevicesMountDevice function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulDevicesMountDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rackId', (done) => {
        try {
          a.rESTfulDevicesMountDevice(null, null, null, (data, error) => {
            try {
              const displayE = 'rackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulDevicesMountDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing containerId', (done) => {
        try {
          a.rESTfulDevicesMountDevice('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'containerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulDevicesMountDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulDevicesMountDevice('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulDevicesMountDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulFreeTextGet - errors', () => {
      it('should have a rESTfulFreeTextGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulFreeTextGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulFreeTextPut - errors', () => {
      it('should have a rESTfulFreeTextPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulFreeTextPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing freeText', (done) => {
        try {
          a.rESTfulFreeTextPut(null, (data, error) => {
            try {
              const displayE = 'freeText is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulFreeTextPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulFreeTextPost - errors', () => {
      it('should have a rESTfulFreeTextPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulFreeTextPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing freeText', (done) => {
        try {
          a.rESTfulFreeTextPost(null, (data, error) => {
            try {
              const displayE = 'freeText is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulFreeTextPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulFreeTextGetById - errors', () => {
      it('should have a rESTfulFreeTextGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulFreeTextGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulFreeTextGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulFreeTextGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulFreeTextDelete - errors', () => {
      it('should have a rESTfulFreeTextDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulFreeTextDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulFreeTextDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulFreeTextDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulFreeTextGetAliases - errors', () => {
      it('should have a rESTfulFreeTextGetAliases function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulFreeTextGetAliases === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing masterFreeTextId', (done) => {
        try {
          a.rESTfulFreeTextGetAliases(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'masterFreeTextId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulFreeTextGetAliases', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWapiV1FreeTextMasterFreeTextIdAliases - errors', () => {
      it('should have a postWapiV1FreeTextMasterFreeTextIdAliases function', (done) => {
        try {
          assert.equal(true, typeof a.postWapiV1FreeTextMasterFreeTextIdAliases === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing masterFreeTextId', (done) => {
        try {
          a.postWapiV1FreeTextMasterFreeTextIdAliases(null, null, (data, error) => {
            try {
              const displayE = 'masterFreeTextId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-postWapiV1FreeTextMasterFreeTextIdAliases', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing freeText', (done) => {
        try {
          a.postWapiV1FreeTextMasterFreeTextIdAliases('fakeparam', null, (data, error) => {
            try {
              const displayE = 'freeText is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-postWapiV1FreeTextMasterFreeTextIdAliases', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulGroupsGet - errors', () => {
      it('should have a rESTfulGroupsGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulGroupsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulGroupsPut - errors', () => {
      it('should have a rESTfulGroupsPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulGroupsPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulGroupsPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulGroupsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulGroupsPost - errors', () => {
      it('should have a rESTfulGroupsPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulGroupsPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing group', (done) => {
        try {
          a.rESTfulGroupsPost(null, (data, error) => {
            try {
              const displayE = 'group is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulGroupsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulGroupsGetById - errors', () => {
      it('should have a rESTfulGroupsGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulGroupsGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulGroupsGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulGroupsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1GroupsId - errors', () => {
      it('should have a putWapiV1GroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1GroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1GroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1GroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1GroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1GroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulGroupsDelete - errors', () => {
      it('should have a rESTfulGroupsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulGroupsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulGroupsDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulGroupsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulHierarchyGet - errors', () => {
      it('should have a rESTfulHierarchyGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulHierarchyGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulHierarchyGetById - errors', () => {
      it('should have a rESTfulHierarchyGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulHierarchyGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulHierarchyGetById(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulHierarchyGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDeviceQueryImportImportDeviceQuery - errors', () => {
      it('should have a rESTfulDeviceQueryImportImportDeviceQuery function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulDeviceQueryImportImportDeviceQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkCategoriesGet - errors', () => {
      it('should have a rESTfulLinkCategoriesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkCategoriesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkCategoriesPut - errors', () => {
      it('should have a rESTfulLinkCategoriesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkCategoriesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulLinkCategoriesPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkCategoriesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkCategoriesPost - errors', () => {
      it('should have a rESTfulLinkCategoriesPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkCategoriesPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulLinkCategoriesPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkCategoriesPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkCategoriesGetById - errors', () => {
      it('should have a rESTfulLinkCategoriesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkCategoriesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulLinkCategoriesGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkCategoriesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1LinkCategoriesId - errors', () => {
      it('should have a putWapiV1LinkCategoriesId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1LinkCategoriesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1LinkCategoriesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinkCategoriesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1LinkCategoriesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinkCategoriesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkCategoriesDelete - errors', () => {
      it('should have a rESTfulLinkCategoriesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkCategoriesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulLinkCategoriesDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkCategoriesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkOverridesGet - errors', () => {
      it('should have a rESTfulLinkOverridesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkOverridesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkOverridesPut - errors', () => {
      it('should have a rESTfulLinkOverridesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkOverridesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulLinkOverridesPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkOverridesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkOverridesPost - errors', () => {
      it('should have a rESTfulLinkOverridesPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkOverridesPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulLinkOverridesPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkOverridesPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkOverridesGetById - errors', () => {
      it('should have a rESTfulLinkOverridesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkOverridesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulLinkOverridesGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkOverridesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverrides - errors', () => {
      it('should have a getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverrides function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverrides === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkTypePropertyId', (done) => {
        try {
          a.getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverrides(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'linkTypePropertyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverrides', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverridesId - errors', () => {
      it('should have a getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverridesId function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverridesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkTypePropertyId', (done) => {
        try {
          a.getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverridesId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'linkTypePropertyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverridesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverridesId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverridesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides - errors', () => {
      it('should have a getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkTypeId', (done) => {
        try {
          a.getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'linkTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkTypePropertyId', (done) => {
        try {
          a.getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'linkTypePropertyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides - errors', () => {
      it('should have a postWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides function', (done) => {
        try {
          assert.equal(true, typeof a.postWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkTypeId', (done) => {
        try {
          a.postWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides(null, null, null, (data, error) => {
            try {
              const displayE = 'linkTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-postWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkTypePropertyId', (done) => {
        try {
          a.postWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'linkTypePropertyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-postWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-postWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId - errors', () => {
      it('should have a getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkTypeId', (done) => {
        try {
          a.getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'linkTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkTypePropertyId', (done) => {
        try {
          a.getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'linkTypePropertyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId - errors', () => {
      it('should have a putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkTypeId', (done) => {
        try {
          a.putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'linkTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkTypePropertyId', (done) => {
        try {
          a.putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'linkTypePropertyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkOverridesDelete - errors', () => {
      it('should have a rESTfulLinkOverridesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkOverridesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulLinkOverridesDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkOverridesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkTypeId', (done) => {
        try {
          a.rESTfulLinkOverridesDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'linkTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkOverridesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkTypePropertyId', (done) => {
        try {
          a.rESTfulLinkOverridesDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkTypePropertyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkOverridesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkPropertyValuesGet - errors', () => {
      it('should have a rESTfulLinkPropertyValuesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkPropertyValuesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkPropertyValuesPut - errors', () => {
      it('should have a rESTfulLinkPropertyValuesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkPropertyValuesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulLinkPropertyValuesPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkPropertyValuesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkPropertyValuesGetById - errors', () => {
      it('should have a rESTfulLinkPropertyValuesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkPropertyValuesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulLinkPropertyValuesGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkPropertyValuesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1LinksLinkIdLinkPropertyValues - errors', () => {
      it('should have a getWapiV1LinksLinkIdLinkPropertyValues function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1LinksLinkIdLinkPropertyValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.getWapiV1LinksLinkIdLinkPropertyValues(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1LinksLinkIdLinkPropertyValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1LinksLinkIdLinkPropertyValues - errors', () => {
      it('should have a putWapiV1LinksLinkIdLinkPropertyValues function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1LinksLinkIdLinkPropertyValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.putWapiV1LinksLinkIdLinkPropertyValues(null, null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinksLinkIdLinkPropertyValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1LinksLinkIdLinkPropertyValues('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinksLinkIdLinkPropertyValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1LinksLinkIdLinkPropertyValuesId - errors', () => {
      it('should have a getWapiV1LinksLinkIdLinkPropertyValuesId function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1LinksLinkIdLinkPropertyValuesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.getWapiV1LinksLinkIdLinkPropertyValuesId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1LinksLinkIdLinkPropertyValuesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getWapiV1LinksLinkIdLinkPropertyValuesId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1LinksLinkIdLinkPropertyValuesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1LinksLinkIdLinkPropertyValuesId - errors', () => {
      it('should have a putWapiV1LinksLinkIdLinkPropertyValuesId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1LinksLinkIdLinkPropertyValuesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1LinksLinkIdLinkPropertyValuesId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinksLinkIdLinkPropertyValuesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.putWapiV1LinksLinkIdLinkPropertyValuesId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinksLinkIdLinkPropertyValuesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1LinksLinkIdLinkPropertyValuesId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinksLinkIdLinkPropertyValuesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkPropertyValuesInitAbsentValues - errors', () => {
      it('should have a rESTfulLinkPropertyValuesInitAbsentValues function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkPropertyValuesInitAbsentValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.rESTfulLinkPropertyValuesInitAbsentValues(null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkPropertyValuesInitAbsentValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkTypePropertiesGet - errors', () => {
      it('should have a rESTfulLinkTypePropertiesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkTypePropertiesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkTypePropertiesPut - errors', () => {
      it('should have a rESTfulLinkTypePropertiesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkTypePropertiesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulLinkTypePropertiesPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkTypePropertiesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkTypePropertiesPost - errors', () => {
      it('should have a rESTfulLinkTypePropertiesPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkTypePropertiesPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulLinkTypePropertiesPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkTypePropertiesPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkTypePropertiesGetById - errors', () => {
      it('should have a rESTfulLinkTypePropertiesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkTypePropertiesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulLinkTypePropertiesGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkTypePropertiesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1LinkTypesLinkTypeIdLinkTypeProperties - errors', () => {
      it('should have a getWapiV1LinkTypesLinkTypeIdLinkTypeProperties function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1LinkTypesLinkTypeIdLinkTypeProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkTypeId', (done) => {
        try {
          a.getWapiV1LinkTypesLinkTypeIdLinkTypeProperties(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'linkTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1LinkTypesLinkTypeIdLinkTypeProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1LinkTypesLinkTypeIdLinkTypeProperties - errors', () => {
      it('should have a putWapiV1LinkTypesLinkTypeIdLinkTypeProperties function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1LinkTypesLinkTypeIdLinkTypeProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkTypeId', (done) => {
        try {
          a.putWapiV1LinkTypesLinkTypeIdLinkTypeProperties(null, null, (data, error) => {
            try {
              const displayE = 'linkTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinkTypesLinkTypeIdLinkTypeProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1LinkTypesLinkTypeIdLinkTypeProperties('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinkTypesLinkTypeIdLinkTypeProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWapiV1LinkTypesLinkTypeIdLinkTypeProperties - errors', () => {
      it('should have a postWapiV1LinkTypesLinkTypeIdLinkTypeProperties function', (done) => {
        try {
          assert.equal(true, typeof a.postWapiV1LinkTypesLinkTypeIdLinkTypeProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkTypeId', (done) => {
        try {
          a.postWapiV1LinkTypesLinkTypeIdLinkTypeProperties(null, null, (data, error) => {
            try {
              const displayE = 'linkTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-postWapiV1LinkTypesLinkTypeIdLinkTypeProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postWapiV1LinkTypesLinkTypeIdLinkTypeProperties('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-postWapiV1LinkTypesLinkTypeIdLinkTypeProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId - errors', () => {
      it('should have a getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkTypeId', (done) => {
        try {
          a.getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'linkTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId - errors', () => {
      it('should have a putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkTypeId', (done) => {
        try {
          a.putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'linkTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkTypePropertiesDelete - errors', () => {
      it('should have a rESTfulLinkTypePropertiesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkTypePropertiesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulLinkTypePropertiesDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkTypePropertiesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkTypeId', (done) => {
        try {
          a.rESTfulLinkTypePropertiesDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkTypePropertiesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkTypesGet - errors', () => {
      it('should have a rESTfulLinkTypesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkTypesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkTypesPut - errors', () => {
      it('should have a rESTfulLinkTypesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkTypesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulLinkTypesPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkTypesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkTypesPost - errors', () => {
      it('should have a rESTfulLinkTypesPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkTypesPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulLinkTypesPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkTypesPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkTypesGetById - errors', () => {
      it('should have a rESTfulLinkTypesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkTypesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulLinkTypesGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkTypesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1LinkTypesId - errors', () => {
      it('should have a putWapiV1LinkTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1LinkTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1LinkTypesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinkTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1LinkTypesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1LinkTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulLinkTypesDelete - errors', () => {
      it('should have a rESTfulLinkTypesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulLinkTypesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulLinkTypesDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulLinkTypesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisLinksGet - errors', () => {
      it('should have a rESTfulVisLinksGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVisLinksGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.rESTfulVisLinksGet(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisLinksGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisLinksPut - errors', () => {
      it('should have a rESTfulVisLinksPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVisLinksPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing visLink', (done) => {
        try {
          a.rESTfulVisLinksPut(null, null, (data, error) => {
            try {
              const displayE = 'visLink is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisLinksPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.rESTfulVisLinksPut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisLinksPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisLinksGetById - errors', () => {
      it('should have a rESTfulVisLinksGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVisLinksGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.rESTfulVisLinksGetById(null, null, null, null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisLinksGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulVisLinksGetById('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisLinksGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeCategoriesGet - errors', () => {
      it('should have a rESTfulNodeCategoriesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeCategoriesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeCategoriesPut - errors', () => {
      it('should have a rESTfulNodeCategoriesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeCategoriesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulNodeCategoriesPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeCategoriesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeCategoriesPost - errors', () => {
      it('should have a rESTfulNodeCategoriesPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeCategoriesPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulNodeCategoriesPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeCategoriesPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeCategoriesGetById - errors', () => {
      it('should have a rESTfulNodeCategoriesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeCategoriesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulNodeCategoriesGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeCategoriesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1NodeCategoriesId - errors', () => {
      it('should have a putWapiV1NodeCategoriesId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1NodeCategoriesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1NodeCategoriesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodeCategoriesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1NodeCategoriesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodeCategoriesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeCategoriesDelete - errors', () => {
      it('should have a rESTfulNodeCategoriesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeCategoriesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulNodeCategoriesDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeCategoriesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeOverridesGet - errors', () => {
      it('should have a rESTfulNodeOverridesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeOverridesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeOverridesPut - errors', () => {
      it('should have a rESTfulNodeOverridesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeOverridesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulNodeOverridesPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeOverridesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeOverridesPost - errors', () => {
      it('should have a rESTfulNodeOverridesPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeOverridesPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulNodeOverridesPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeOverridesPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeOverridesGetById - errors', () => {
      it('should have a rESTfulNodeOverridesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeOverridesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulNodeOverridesGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeOverridesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverrides - errors', () => {
      it('should have a getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverrides function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverrides === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeTypePropertyId', (done) => {
        try {
          a.getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverrides(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeTypePropertyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverrides', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverridesId - errors', () => {
      it('should have a getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverridesId function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverridesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeTypePropertyId', (done) => {
        try {
          a.getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverridesId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeTypePropertyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverridesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverridesId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverridesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides - errors', () => {
      it('should have a getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeTypeId', (done) => {
        try {
          a.getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeTypePropertyId', (done) => {
        try {
          a.getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeTypePropertyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides - errors', () => {
      it('should have a postWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides function', (done) => {
        try {
          assert.equal(true, typeof a.postWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeTypeId', (done) => {
        try {
          a.postWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides(null, null, null, (data, error) => {
            try {
              const displayE = 'nodeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-postWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeTypePropertyId', (done) => {
        try {
          a.postWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeTypePropertyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-postWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-postWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId - errors', () => {
      it('should have a getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeTypeId', (done) => {
        try {
          a.getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeTypePropertyId', (done) => {
        try {
          a.getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeTypePropertyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId - errors', () => {
      it('should have a putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeTypeId', (done) => {
        try {
          a.putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'nodeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeTypePropertyId', (done) => {
        try {
          a.putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeTypePropertyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeOverridesDelete - errors', () => {
      it('should have a rESTfulNodeOverridesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeOverridesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulNodeOverridesDelete(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeOverridesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeTypeId', (done) => {
        try {
          a.rESTfulNodeOverridesDelete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeOverridesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeTypePropertyId', (done) => {
        try {
          a.rESTfulNodeOverridesDelete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeTypePropertyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeOverridesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodePropertyValuesGet - errors', () => {
      it('should have a rESTfulNodePropertyValuesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodePropertyValuesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodePropertyValuesPut - errors', () => {
      it('should have a rESTfulNodePropertyValuesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodePropertyValuesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulNodePropertyValuesPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodePropertyValuesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodePropertyValuesGetById - errors', () => {
      it('should have a rESTfulNodePropertyValuesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodePropertyValuesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulNodePropertyValuesGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodePropertyValuesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1NodesNodeIdNodePropertyValues - errors', () => {
      it('should have a getWapiV1NodesNodeIdNodePropertyValues function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1NodesNodeIdNodePropertyValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.getWapiV1NodesNodeIdNodePropertyValues(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1NodesNodeIdNodePropertyValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1NodesNodeIdNodePropertyValues - errors', () => {
      it('should have a putWapiV1NodesNodeIdNodePropertyValues function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1NodesNodeIdNodePropertyValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.putWapiV1NodesNodeIdNodePropertyValues(null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodesNodeIdNodePropertyValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1NodesNodeIdNodePropertyValues('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodesNodeIdNodePropertyValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1NodesNodeIdNodePropertyValuesId - errors', () => {
      it('should have a getWapiV1NodesNodeIdNodePropertyValuesId function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1NodesNodeIdNodePropertyValuesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.getWapiV1NodesNodeIdNodePropertyValuesId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1NodesNodeIdNodePropertyValuesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getWapiV1NodesNodeIdNodePropertyValuesId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1NodesNodeIdNodePropertyValuesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1NodesNodeIdNodePropertyValuesId - errors', () => {
      it('should have a putWapiV1NodesNodeIdNodePropertyValuesId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1NodesNodeIdNodePropertyValuesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1NodesNodeIdNodePropertyValuesId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodesNodeIdNodePropertyValuesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.putWapiV1NodesNodeIdNodePropertyValuesId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodesNodeIdNodePropertyValuesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1NodesNodeIdNodePropertyValuesId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodesNodeIdNodePropertyValuesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodePropertyValuesInitAbsentValues - errors', () => {
      it('should have a rESTfulNodePropertyValuesInitAbsentValues function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodePropertyValuesInitAbsentValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.rESTfulNodePropertyValuesInitAbsentValues(null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodePropertyValuesInitAbsentValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeTypePropertiesGet - errors', () => {
      it('should have a rESTfulNodeTypePropertiesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeTypePropertiesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeTypePropertiesPut - errors', () => {
      it('should have a rESTfulNodeTypePropertiesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeTypePropertiesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulNodeTypePropertiesPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeTypePropertiesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeTypePropertiesPost - errors', () => {
      it('should have a rESTfulNodeTypePropertiesPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeTypePropertiesPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulNodeTypePropertiesPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeTypePropertiesPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeTypePropertiesGetById - errors', () => {
      it('should have a rESTfulNodeTypePropertiesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeTypePropertiesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulNodeTypePropertiesGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeTypePropertiesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1NodeTypesNodeTypeIdNodeTypeProperties - errors', () => {
      it('should have a getWapiV1NodeTypesNodeTypeIdNodeTypeProperties function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1NodeTypesNodeTypeIdNodeTypeProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeTypeId', (done) => {
        try {
          a.getWapiV1NodeTypesNodeTypeIdNodeTypeProperties(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1NodeTypesNodeTypeIdNodeTypeProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1NodeTypesNodeTypeIdNodeTypeProperties - errors', () => {
      it('should have a putWapiV1NodeTypesNodeTypeIdNodeTypeProperties function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1NodeTypesNodeTypeIdNodeTypeProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeTypeId', (done) => {
        try {
          a.putWapiV1NodeTypesNodeTypeIdNodeTypeProperties(null, null, (data, error) => {
            try {
              const displayE = 'nodeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodeTypesNodeTypeIdNodeTypeProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1NodeTypesNodeTypeIdNodeTypeProperties('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodeTypesNodeTypeIdNodeTypeProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWapiV1NodeTypesNodeTypeIdNodeTypeProperties - errors', () => {
      it('should have a postWapiV1NodeTypesNodeTypeIdNodeTypeProperties function', (done) => {
        try {
          assert.equal(true, typeof a.postWapiV1NodeTypesNodeTypeIdNodeTypeProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeTypeId', (done) => {
        try {
          a.postWapiV1NodeTypesNodeTypeIdNodeTypeProperties(null, null, (data, error) => {
            try {
              const displayE = 'nodeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-postWapiV1NodeTypesNodeTypeIdNodeTypeProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postWapiV1NodeTypesNodeTypeIdNodeTypeProperties('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-postWapiV1NodeTypesNodeTypeIdNodeTypeProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId - errors', () => {
      it('should have a getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeTypeId', (done) => {
        try {
          a.getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId - errors', () => {
      it('should have a putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeTypeId', (done) => {
        try {
          a.putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeTypePropertiesDelete - errors', () => {
      it('should have a rESTfulNodeTypePropertiesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeTypePropertiesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulNodeTypePropertiesDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeTypePropertiesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeTypeId', (done) => {
        try {
          a.rESTfulNodeTypePropertiesDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeTypePropertiesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeTypesGet - errors', () => {
      it('should have a rESTfulNodeTypesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeTypesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeTypesPut - errors', () => {
      it('should have a rESTfulNodeTypesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeTypesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulNodeTypesPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeTypesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeTypesPost - errors', () => {
      it('should have a rESTfulNodeTypesPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeTypesPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulNodeTypesPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeTypesPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeTypesGetById - errors', () => {
      it('should have a rESTfulNodeTypesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeTypesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulNodeTypesGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeTypesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1NodeTypesId - errors', () => {
      it('should have a putWapiV1NodeTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1NodeTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1NodeTypesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodeTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1NodeTypesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodeTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeTypesDelete - errors', () => {
      it('should have a rESTfulNodeTypesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeTypesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulNodeTypesDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeTypesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodesGet - errors', () => {
      it('should have a rESTfulNodesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodesPut - errors', () => {
      it('should have a rESTfulNodesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulNodesPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodesPost - errors', () => {
      it('should have a rESTfulNodesPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodesPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulNodesPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodesPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodesGetById - errors', () => {
      it('should have a rESTfulNodesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulNodesGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1NodesId - errors', () => {
      it('should have a putWapiV1NodesId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1NodesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1NodesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1NodesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodesDelete - errors', () => {
      it('should have a rESTfulNodesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulNodesDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodesGetAliases - errors', () => {
      it('should have a rESTfulNodesGetAliases function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodesGetAliases === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing masterNodeId', (done) => {
        try {
          a.rESTfulNodesGetAliases(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'masterNodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodesGetAliases', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodesPostAlias - errors', () => {
      it('should have a rESTfulNodesPostAlias function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodesPostAlias === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing masterNodeId', (done) => {
        try {
          a.rESTfulNodesPostAlias(null, null, (data, error) => {
            try {
              const displayE = 'masterNodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodesPostAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulNodesPostAlias('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodesPostAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1NodesSeparateLinks - errors', () => {
      it('should have a putWapiV1NodesSeparateLinks function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1NodesSeparateLinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing visLinksSepartionData', (done) => {
        try {
          a.putWapiV1NodesSeparateLinks(null, (data, error) => {
            try {
              const displayE = 'visLinksSepartionData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1NodesSeparateLinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDiagramSettingsGetDiagramSettings - errors', () => {
      it('should have a rESTfulDiagramSettingsGetDiagramSettings function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulDiagramSettingsGetDiagramSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulDiagramSettingsGetDiagramSettings(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulDiagramSettingsGetDiagramSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulDiagramSettingsPut - errors', () => {
      it('should have a rESTfulDiagramSettingsPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulDiagramSettingsPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulDiagramSettingsPut(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulDiagramSettingsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulDiagramSettingsPut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulDiagramSettingsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeMapCoordinatesGetMapCoordinates - errors', () => {
      it('should have a rESTfulNodeMapCoordinatesGetMapCoordinates function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeMapCoordinatesGetMapCoordinates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulNodeMapCoordinatesGetMapCoordinates(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeMapCoordinatesGetMapCoordinates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulNodeMapCoordinatesPut - errors', () => {
      it('should have a rESTfulNodeMapCoordinatesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulNodeMapCoordinatesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulNodeMapCoordinatesPut(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeMapCoordinatesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulNodeMapCoordinatesPut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulNodeMapCoordinatesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisNodesGet - errors', () => {
      it('should have a rESTfulVisNodesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVisNodesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.rESTfulVisNodesGet(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisNodesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisNodesPut - errors', () => {
      it('should have a rESTfulVisNodesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVisNodesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing visNode', (done) => {
        try {
          a.rESTfulVisNodesPut(null, null, (data, error) => {
            try {
              const displayE = 'visNode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisNodesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.rESTfulVisNodesPut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisNodesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisNodesGetById - errors', () => {
      it('should have a rESTfulVisNodesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVisNodesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.rESTfulVisNodesGetById(null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisNodesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulVisNodesGetById('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisNodesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisNodesGetOriginal - errors', () => {
      it('should have a rESTfulVisNodesGetOriginal function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVisNodesGetOriginal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.rESTfulVisNodesGetOriginal(null, null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisNodesGetOriginal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisNodesGetReferences - errors', () => {
      it('should have a rESTfulVisNodesGetReferences function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVisNodesGetReferences === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.rESTfulVisNodesGetReferences(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisNodesGetReferences', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRackContainerDesignsGet - errors', () => {
      it('should have a rESTfulRackContainerDesignsGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRackContainerDesignsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRackContainerDesignsPut - errors', () => {
      it('should have a rESTfulRackContainerDesignsPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRackContainerDesignsPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulRackContainerDesignsPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulRackContainerDesignsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRackContainerDesignsPost - errors', () => {
      it('should have a rESTfulRackContainerDesignsPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRackContainerDesignsPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulRackContainerDesignsPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulRackContainerDesignsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRackContainerDesignsGetById - errors', () => {
      it('should have a rESTfulRackContainerDesignsGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRackContainerDesignsGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulRackContainerDesignsGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulRackContainerDesignsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1RackTypesRackTypeIdRackContainerDesigns - errors', () => {
      it('should have a getWapiV1RackTypesRackTypeIdRackContainerDesigns function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1RackTypesRackTypeIdRackContainerDesigns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rackTypeId', (done) => {
        try {
          a.getWapiV1RackTypesRackTypeIdRackContainerDesigns(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'rackTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1RackTypesRackTypeIdRackContainerDesigns', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWapiV1RackTypesRackTypeIdRackContainerDesigns - errors', () => {
      it('should have a postWapiV1RackTypesRackTypeIdRackContainerDesigns function', (done) => {
        try {
          assert.equal(true, typeof a.postWapiV1RackTypesRackTypeIdRackContainerDesigns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rackTypeId', (done) => {
        try {
          a.postWapiV1RackTypesRackTypeIdRackContainerDesigns(null, null, (data, error) => {
            try {
              const displayE = 'rackTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-postWapiV1RackTypesRackTypeIdRackContainerDesigns', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postWapiV1RackTypesRackTypeIdRackContainerDesigns('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-postWapiV1RackTypesRackTypeIdRackContainerDesigns', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1RackTypesRackTypeIdRackContainerDesignsId - errors', () => {
      it('should have a getWapiV1RackTypesRackTypeIdRackContainerDesignsId function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1RackTypesRackTypeIdRackContainerDesignsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rackTypeId', (done) => {
        try {
          a.getWapiV1RackTypesRackTypeIdRackContainerDesignsId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'rackTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1RackTypesRackTypeIdRackContainerDesignsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getWapiV1RackTypesRackTypeIdRackContainerDesignsId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1RackTypesRackTypeIdRackContainerDesignsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1RackTypesRackTypeIdRackContainerDesignsId - errors', () => {
      it('should have a putWapiV1RackTypesRackTypeIdRackContainerDesignsId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1RackTypesRackTypeIdRackContainerDesignsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1RackTypesRackTypeIdRackContainerDesignsId(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1RackTypesRackTypeIdRackContainerDesignsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rackTypeId', (done) => {
        try {
          a.putWapiV1RackTypesRackTypeIdRackContainerDesignsId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'rackTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1RackTypesRackTypeIdRackContainerDesignsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1RackTypesRackTypeIdRackContainerDesignsId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1RackTypesRackTypeIdRackContainerDesignsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRackContainerDesignsDelete - errors', () => {
      it('should have a rESTfulRackContainerDesignsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRackContainerDesignsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulRackContainerDesignsDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulRackContainerDesignsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rackTypeId', (done) => {
        try {
          a.rESTfulRackContainerDesignsDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'rackTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulRackContainerDesignsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRackTypesGet - errors', () => {
      it('should have a rESTfulRackTypesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRackTypesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRackTypesPut - errors', () => {
      it('should have a rESTfulRackTypesPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRackTypesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulRackTypesPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulRackTypesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRackTypesPost - errors', () => {
      it('should have a rESTfulRackTypesPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRackTypesPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulRackTypesPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulRackTypesPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRackTypesGetById - errors', () => {
      it('should have a rESTfulRackTypesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRackTypesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulRackTypesGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulRackTypesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1RackTypesId - errors', () => {
      it('should have a putWapiV1RackTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1RackTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1RackTypesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1RackTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1RackTypesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1RackTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRackTypesDelete - errors', () => {
      it('should have a rESTfulRackTypesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRackTypesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulRackTypesDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulRackTypesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRacksGet - errors', () => {
      it('should have a rESTfulRacksGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRacksGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRacksPost - errors', () => {
      it('should have a rESTfulRacksPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRacksPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulRacksPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulRacksPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRacksGetById - errors', () => {
      it('should have a rESTfulRacksGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRacksGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulRacksGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulRacksGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRacksPut - errors', () => {
      it('should have a rESTfulRacksPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRacksPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulRacksPut(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulRacksPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulRacksPut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulRacksPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRacksDelete - errors', () => {
      it('should have a rESTfulRacksDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRacksDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulRacksDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulRacksDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRackContainersGet - errors', () => {
      it('should have a rESTfulRackContainersGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRackContainersGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rackId', (done) => {
        try {
          a.rESTfulRackContainersGet(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'rackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulRackContainersGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRackContainersGetById - errors', () => {
      it('should have a rESTfulRackContainersGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRackContainersGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rackId', (done) => {
        try {
          a.rESTfulRackContainersGetById(null, null, null, null, (data, error) => {
            try {
              const displayE = 'rackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulRackContainersGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulRackContainersGetById('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulRackContainersGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRolesGet - errors', () => {
      it('should have a rESTfulRolesGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRolesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulRolesGetById - errors', () => {
      it('should have a rESTfulRolesGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulRolesGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulRolesGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulRolesGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulSearchGet - errors', () => {
      it('should have a rESTfulSearchGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulSearchGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulWorkOrderTasksGet - errors', () => {
      it('should have a rESTfulWorkOrderTasksGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulWorkOrderTasksGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiV1WorkOrdersWorkOrderIdTasks - errors', () => {
      it('should have a getWapiV1WorkOrdersWorkOrderIdTasks function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiV1WorkOrdersWorkOrderIdTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workOrderId', (done) => {
        try {
          a.getWapiV1WorkOrdersWorkOrderIdTasks(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'workOrderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-getWapiV1WorkOrdersWorkOrderIdTasks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulWorkOrderTasksPut - errors', () => {
      it('should have a rESTfulWorkOrderTasksPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulWorkOrderTasksPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workOrderTask', (done) => {
        try {
          a.rESTfulWorkOrderTasksPut(null, null, (data, error) => {
            try {
              const displayE = 'workOrderTask is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulWorkOrderTasksPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workOrderId', (done) => {
        try {
          a.rESTfulWorkOrderTasksPut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'workOrderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulWorkOrderTasksPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulWorkOrderTasksPost - errors', () => {
      it('should have a rESTfulWorkOrderTasksPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulWorkOrderTasksPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workOrderTask', (done) => {
        try {
          a.rESTfulWorkOrderTasksPost(null, null, (data, error) => {
            try {
              const displayE = 'workOrderTask is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulWorkOrderTasksPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workOrderId', (done) => {
        try {
          a.rESTfulWorkOrderTasksPost('fakeparam', null, (data, error) => {
            try {
              const displayE = 'workOrderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulWorkOrderTasksPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulWorkOrderTasksGetById - errors', () => {
      it('should have a rESTfulWorkOrderTasksGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulWorkOrderTasksGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workOrderId', (done) => {
        try {
          a.rESTfulWorkOrderTasksGetById(null, null, null, null, (data, error) => {
            try {
              const displayE = 'workOrderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulWorkOrderTasksGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulWorkOrderTasksGetById('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulWorkOrderTasksGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulWorkOrderTasksDelete - errors', () => {
      it('should have a rESTfulWorkOrderTasksDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulWorkOrderTasksDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulWorkOrderTasksDelete(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulWorkOrderTasksDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workOrderId', (done) => {
        try {
          a.rESTfulWorkOrderTasksDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'workOrderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulWorkOrderTasksDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulUsersGet - errors', () => {
      it('should have a rESTfulUsersGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulUsersGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulUsersPut - errors', () => {
      it('should have a rESTfulUsersPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulUsersPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulUsersPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulUsersPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulUsersPost - errors', () => {
      it('should have a rESTfulUsersPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulUsersPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rESTfulUsersPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulUsersPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulUsersGetById - errors', () => {
      it('should have a rESTfulUsersGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulUsersGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulUsersGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulUsersGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1UsersId - errors', () => {
      it('should have a putWapiV1UsersId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1UsersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1UsersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1UsersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putWapiV1UsersId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1UsersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulUsersDelete - errors', () => {
      it('should have a rESTfulUsersDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulUsersDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulUsersDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulUsersDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulUsersSetPassword - errors', () => {
      it('should have a rESTfulUsersSetPassword function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulUsersSetPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulUsersSetPassword(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulUsersSetPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing password', (done) => {
        try {
          a.rESTfulUsersSetPassword('fakeparam', null, (data, error) => {
            try {
              const displayE = 'password is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulUsersSetPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVendorsGet - errors', () => {
      it('should have a rESTfulVendorsGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVendorsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVendorsPut - errors', () => {
      it('should have a rESTfulVendorsPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVendorsPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendor', (done) => {
        try {
          a.rESTfulVendorsPut(null, (data, error) => {
            try {
              const displayE = 'vendor is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVendorsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVendorsPost - errors', () => {
      it('should have a rESTfulVendorsPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVendorsPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendor', (done) => {
        try {
          a.rESTfulVendorsPost(null, (data, error) => {
            try {
              const displayE = 'vendor is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVendorsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVendorsGetById - errors', () => {
      it('should have a rESTfulVendorsGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVendorsGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulVendorsGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVendorsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWapiV1VendorsId - errors', () => {
      it('should have a putWapiV1VendorsId function', (done) => {
        try {
          assert.equal(true, typeof a.putWapiV1VendorsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putWapiV1VendorsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1VendorsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendor', (done) => {
        try {
          a.putWapiV1VendorsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vendor is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-putWapiV1VendorsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVendorsDelete - errors', () => {
      it('should have a rESTfulVendorsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVendorsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulVendorsDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVendorsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisLinkDisplayedFieldsGet - errors', () => {
      it('should have a rESTfulVisLinkDisplayedFieldsGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVisLinkDisplayedFieldsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing visLinkId', (done) => {
        try {
          a.rESTfulVisLinkDisplayedFieldsGet(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'visLinkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisLinkDisplayedFieldsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisLinkDisplayedFieldsPut - errors', () => {
      it('should have a rESTfulVisLinkDisplayedFieldsPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVisLinkDisplayedFieldsPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkDisplayedField', (done) => {
        try {
          a.rESTfulVisLinkDisplayedFieldsPut(null, null, (data, error) => {
            try {
              const displayE = 'linkDisplayedField is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisLinkDisplayedFieldsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing visLinkId', (done) => {
        try {
          a.rESTfulVisLinkDisplayedFieldsPut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'visLinkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisLinkDisplayedFieldsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisLinkDisplayedFieldsPost - errors', () => {
      it('should have a rESTfulVisLinkDisplayedFieldsPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVisLinkDisplayedFieldsPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing displayedField', (done) => {
        try {
          a.rESTfulVisLinkDisplayedFieldsPost(null, null, (data, error) => {
            try {
              const displayE = 'displayedField is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisLinkDisplayedFieldsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing visLinkId', (done) => {
        try {
          a.rESTfulVisLinkDisplayedFieldsPost('fakeparam', null, (data, error) => {
            try {
              const displayE = 'visLinkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisLinkDisplayedFieldsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisLinkDisplayedFieldsGetById - errors', () => {
      it('should have a rESTfulVisLinkDisplayedFieldsGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVisLinkDisplayedFieldsGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkPropertyValueId', (done) => {
        try {
          a.rESTfulVisLinkDisplayedFieldsGetById(null, null, null, null, (data, error) => {
            try {
              const displayE = 'linkPropertyValueId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisLinkDisplayedFieldsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing visLinkId', (done) => {
        try {
          a.rESTfulVisLinkDisplayedFieldsGetById('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'visLinkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisLinkDisplayedFieldsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisNodeDisplayedFieldsGet - errors', () => {
      it('should have a rESTfulVisNodeDisplayedFieldsGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVisNodeDisplayedFieldsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing visNodeId', (done) => {
        try {
          a.rESTfulVisNodeDisplayedFieldsGet(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'visNodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisNodeDisplayedFieldsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisNodeDisplayedFieldsPut - errors', () => {
      it('should have a rESTfulVisNodeDisplayedFieldsPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVisNodeDisplayedFieldsPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeDisplayedField', (done) => {
        try {
          a.rESTfulVisNodeDisplayedFieldsPut(null, null, (data, error) => {
            try {
              const displayE = 'nodeDisplayedField is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisNodeDisplayedFieldsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing visNodeId', (done) => {
        try {
          a.rESTfulVisNodeDisplayedFieldsPut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'visNodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisNodeDisplayedFieldsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisNodeDisplayedFieldsPost - errors', () => {
      it('should have a rESTfulVisNodeDisplayedFieldsPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVisNodeDisplayedFieldsPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing displayedField', (done) => {
        try {
          a.rESTfulVisNodeDisplayedFieldsPost(null, null, (data, error) => {
            try {
              const displayE = 'displayedField is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisNodeDisplayedFieldsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing visNodeId', (done) => {
        try {
          a.rESTfulVisNodeDisplayedFieldsPost('fakeparam', null, (data, error) => {
            try {
              const displayE = 'visNodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisNodeDisplayedFieldsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulVisNodeDisplayedFieldsGetById - errors', () => {
      it('should have a rESTfulVisNodeDisplayedFieldsGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulVisNodeDisplayedFieldsGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodePropertyValueId', (done) => {
        try {
          a.rESTfulVisNodeDisplayedFieldsGetById(null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodePropertyValueId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisNodeDisplayedFieldsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing visNodeId', (done) => {
        try {
          a.rESTfulVisNodeDisplayedFieldsGetById('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'visNodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulVisNodeDisplayedFieldsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulWorkOrdersGet - errors', () => {
      it('should have a rESTfulWorkOrdersGet function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulWorkOrdersGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulWorkOrdersPut - errors', () => {
      it('should have a rESTfulWorkOrdersPut function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulWorkOrdersPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workOrder', (done) => {
        try {
          a.rESTfulWorkOrdersPut(null, (data, error) => {
            try {
              const displayE = 'workOrder is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulWorkOrdersPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulWorkOrdersPost - errors', () => {
      it('should have a rESTfulWorkOrdersPost function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulWorkOrdersPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workOrder', (done) => {
        try {
          a.rESTfulWorkOrdersPost(null, (data, error) => {
            try {
              const displayE = 'workOrder is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulWorkOrdersPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulWorkOrdersGetById - errors', () => {
      it('should have a rESTfulWorkOrdersGetById function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulWorkOrdersGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulWorkOrdersGetById(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulWorkOrdersGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rESTfulWorkOrdersDelete - errors', () => {
      it('should have a rESTfulWorkOrdersDelete function', (done) => {
        try {
          assert.equal(true, typeof a.rESTfulWorkOrdersDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rESTfulWorkOrdersDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-netterrain-adapter-rESTfulWorkOrdersDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
