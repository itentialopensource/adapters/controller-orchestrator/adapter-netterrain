## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Netterrain. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Netterrain.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the NetTerrain. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCardsGet(parentTypeGroup, parentId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the cards collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/cards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1Cards(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the cards collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/cards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCardsGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the card.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/cards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCardsPut(id, body, callback)</td>
    <td style="padding:15px">Updates an existing card.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/cards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCardsDelete(id, callback)</td>
    <td style="padding:15px">Deletes a card.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/cards/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulPortsGet(typeGroup, nodeId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the ports collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulPortsGetById(typeGroup, nodeId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the port.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/ports/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1Ports(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the ports collection with no relation to a specific device or card.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinksGet(typeGroup, nodeId, id, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the collection of links linked to a specific port.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/ports/{pathv3}/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1Links(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the links collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinksPut(body, callback)</td>
    <td style="padding:15px">Updates an existing link.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinksPost(body, callback)</td>
    <td style="padding:15px">Creates a link.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinksGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the link.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/links/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1LinksId(id, body, callback)</td>
    <td style="padding:15px">Updates an existing link.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/links/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinksDelete(id, callback)</td>
    <td style="padding:15px">Deletes a link.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/links/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulSlotsGet(typeGroup, nodeId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the slots collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/slots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulSlotsGetById(typeGroup, nodeId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the slot.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/slots/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulSlotsPost(typeGroup, nodeId, id, body, callback)</td>
    <td style="padding:15px">Transforms the slot to the card (installs the card into the slot).</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/slots/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1Slots(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the slots collection with no relation to a specific device or card.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/slots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulPortRepresentationsGet(typeGroup, typeId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the port representations collection for a type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/port_representations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulPortRepresentationsPost(typeGroup, typeId, body, callback)</td>
    <td style="padding:15px">Creates an new card or device port representation.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/port_representations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulPortRepresentationsGetById(typeGroup, typeId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the port representation belonging to the card or device type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/port_representations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulPortRepresentationsPut(typeGroup, typeId, id, body, callback)</td>
    <td style="padding:15px">Update a card type's port representation.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/port_representations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulPortRepresentationsDelete(typeGroup, typeId, id, callback)</td>
    <td style="padding:15px">Deletes the card type's port representation.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/port_representations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1PortRepresentations(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the port representations collection with no relation to a specific a type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/port_representations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1PortRepresentations(body, callback)</td>
    <td style="padding:15px">Update a card type's port representation.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/port_representations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWapiV1PortRepresentations(body, callback)</td>
    <td style="padding:15px">Creates an new card type's port representation.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/port_representations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1PortRepresentationsId(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the port representation belonging to the card type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/port_representations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulPortFieldsGet(portRepresentationId, typeGroup, typeId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the port fields collection for a port representation.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/port_representations/{pathv3}/port_fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulPortFieldsGetById(typeGroup, typeId, portRepresentationId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the port field by id.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/port_representations/{pathv3}/port_fields/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1PortFields(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the general port fields collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/port_fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1PortFieldsId(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the port field by id.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/port_fields/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulSlotRepresentationsGet(typeGroup, typeId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the slot eepresentations collection for a type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/slot_representations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulSlotRepresentationsPost(typeGroup, typeId, body, callback)</td>
    <td style="padding:15px">Creates an new card or device slot representation.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/slot_representations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulSlotRepresentationsGetById(typeGroup, typeId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the slot representation belonging to the card or device type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/slot_representations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulSlotRepresentationsPut(typeGroup, typeId, id, body, callback)</td>
    <td style="padding:15px">Update a card type's slot representation.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/slot_representations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulSlotRepresentationsDelete(typeGroup, typeId, id, callback)</td>
    <td style="padding:15px">Deletes the card type's slot representation.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/slot_representations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1SlotRepresentations(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the slot representations collection with no relation to a specific a type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/slot_representations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1SlotRepresentations(body, callback)</td>
    <td style="padding:15px">Update a card type's slot representation.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/slot_representations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWapiV1SlotRepresentations(body, callback)</td>
    <td style="padding:15px">Creates an new card type's slot representation.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/slot_representations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1SlotRepresentationsId(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the slot representation belonging to the card type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/slot_representations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulSlotFieldsGet(slotRepresentationId, typeGroup, typeId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the slot fields collection for a slot representation.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/slot_representations/{pathv3}/slot_fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulSlotFieldsGetById(typeGroup, typeId, slotRepresentationId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the slot field by id.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/slot_representations/{pathv3}/slot_fields/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1SlotFields(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the general slot fields collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/slot_fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1SlotFieldsId(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the slot field by id.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/slot_fields/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulSlotMappingGet(slotRepresentationId, typeGroup, typeId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the slot mappings collection for a slot representation.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/slot_representations/{pathv3}/slot_mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulSlotMappingPost(typeGroup, typeId, slotRepresentationId, body, callback)</td>
    <td style="padding:15px">Creates an new card or device slot mapping.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/slot_representations/{pathv3}/slot_mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulSlotMappingGetById(typeGroup, typeId, slotRepresentationId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the slot mapping by id.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/slot_representations/{pathv3}/slot_mapping/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulSlotMappingDelete(typeGroup, typeId, slotRepresentationId, id, callback)</td>
    <td style="padding:15px">Deletes the card type's slot mapping.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/{pathv1}/{pathv2}/slot_representations/{pathv3}/slot_mapping/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1SlotMapping(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the general slot mappings collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/slot_mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWapiV1SlotMapping(body, callback)</td>
    <td style="padding:15px">Creates an new slot mapping.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/slot_mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1SlotMappingId(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the slot mapping by id.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/slot_mapping/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCardTypesGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the card types collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/card_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCardTypesPut(body, callback)</td>
    <td style="padding:15px">Updates an existing card type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/card_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCardTypesPost(body, callback)</td>
    <td style="padding:15px">Creates an new card type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/card_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCardTypesGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the card type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/card_types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1CardTypesId(id, body, callback)</td>
    <td style="padding:15px">Updates an existing card type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/card_types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCardTypesDelete(id, callback)</td>
    <td style="padding:15px">Deletes a card type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/card_types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCircuitPropertiesGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the circuit properties collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/circuit_properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCircuitPropertiesPut(property, callback)</td>
    <td style="padding:15px">Updates an existing circuit property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/circuit_properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCircuitPropertiesPost(circuitProperty, callback)</td>
    <td style="padding:15px">Creates a new circuit property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/circuit_properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCircuitPropertiesGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a circuit property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/circuit_properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCircuitPropertiesDelete(id, callback)</td>
    <td style="padding:15px">Deletes a circuit property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/circuit_properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCircuitPropertyValuesGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the circuit property values collection with no relation to a specific circuit.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/circuit_property_values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1CircuitsCircuitIdCircuitPropertyValues(circuitId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the circuit property values collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/circuits/{pathv1}/circuit_property_values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCircuitPropertyValuesPut(circuitId, circuitPropertyValue, callback)</td>
    <td style="padding:15px">Updates an existing circuit property value.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/circuits/{pathv1}/circuit_property_values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCircuitPropertyValuesGetById(circuitId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the circuit property value.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/circuits/{pathv1}/circuit_property_values/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCircuitPropertyValuesInitAbsentValues(circuitId, callback)</td>
    <td style="padding:15px">Creates absent circuit property values.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/circuits/{pathv1}/circuit_property_values/init_absent_values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCircuitsGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the circuits collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/circuits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCircuitsPost(addCircuitData, callback)</td>
    <td style="padding:15px">Creates a circuit.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/circuits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCircuitsGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the circuit.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/circuits/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulCircuitsDelete(id, callback)</td>
    <td style="padding:15px">Deletes a circuit.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/circuits/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulDeviceTypesGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the device types collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/device_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulDeviceTypesPut(body, callback)</td>
    <td style="padding:15px">Updates an existing device type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/device_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulDeviceTypesPost(body, callback)</td>
    <td style="padding:15px">Creates an new device type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/device_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulDeviceTypesGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the device type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/device_types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1DeviceTypesId(id, body, callback)</td>
    <td style="padding:15px">Updates an existing device type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/device_types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulDeviceTypesDelete(id, callback)</td>
    <td style="padding:15px">Deletes a device type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/device_types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulDevicesGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the devices collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulDevicesPost(body, callback)</td>
    <td style="padding:15px">Creates a device.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulDevicesGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the device.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulDevicesPut(id, body, callback)</td>
    <td style="padding:15px">Updates an existing device.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulDevicesDelete(id, callback)</td>
    <td style="padding:15px">Deletes a device.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1RacksRackIdContainersContainerIdMountedDevices(rackId, containerId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the collection of devices mounted to the rack container.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/racks/{pathv1}/containers/{pathv2}/mounted_devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulDevicesMountDevice(rackId, containerId, body, callback)</td>
    <td style="padding:15px">Mounts the device into the rack container.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/racks/{pathv1}/containers/{pathv2}/mounted_devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulFreeTextGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the free text objects collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/free_text?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulFreeTextPut(freeText, callback)</td>
    <td style="padding:15px">Updates an existing free text object.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/free_text?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulFreeTextPost(freeText, callback)</td>
    <td style="padding:15px">Creates a free text object.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/free_text?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulFreeTextGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the free text object.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/free_text/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulFreeTextDelete(id, callback)</td>
    <td style="padding:15px">Deletes a free text object.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/free_text/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulFreeTextGetAliases(masterFreeTextId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the collection of free text objct aliases.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/free_text/{pathv1}/aliases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWapiV1FreeTextMasterFreeTextIdAliases(masterFreeTextId, freeText, callback)</td>
    <td style="padding:15px">Creates a free text alias.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/free_text/{pathv1}/aliases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulGroupsGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the user groups collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulGroupsPut(body, callback)</td>
    <td style="padding:15px">Updates an existing user group.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulGroupsPost(group, callback)</td>
    <td style="padding:15px">Creates a user group.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulGroupsGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a user group.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1GroupsId(id, body, callback)</td>
    <td style="padding:15px">Updates an existing user group.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulGroupsDelete(id, callback)</td>
    <td style="padding:15px">Deletes a user group.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulHierarchyGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the hierarchy.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/hierarchy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulHierarchyGetById(id, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the hierarchy starting from the root element.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/hierarchy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulDeviceQueryImportImportDeviceQuery(callback)</td>
    <td style="padding:15px">Imports the zipped device type query and creates the device type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/import/device_type_query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkCategoriesGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the link categories collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkCategoriesPut(body, callback)</td>
    <td style="padding:15px">Updates an existing link category.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkCategoriesPost(body, callback)</td>
    <td style="padding:15px">Creates a link category.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkCategoriesGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a link category.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_categories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1LinkCategoriesId(id, body, callback)</td>
    <td style="padding:15px">Updates an existing link category.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_categories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkCategoriesDelete(id, callback)</td>
    <td style="padding:15px">Deletes a link category.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_categories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkOverridesGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the general link overrides collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkOverridesPut(body, callback)</td>
    <td style="padding:15px">Updates an existing link override.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkOverridesPost(body, callback)</td>
    <td style="padding:15px">Creates a link override.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkOverridesGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a link override.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_overrides/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverrides(linkTypePropertyId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the link overrides collection for a property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_type_properties/{pathv1}/link_overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1LinkTypePropertiesLinkTypePropertyIdLinkOverridesId(linkTypePropertyId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a link override.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_type_properties/{pathv1}/link_overrides/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides(linkTypeId, linkTypePropertyId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the link overrides collection for a property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_types/{pathv1}/link_type_properties/{pathv2}/link_overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverrides(linkTypeId, linkTypePropertyId, body, callback)</td>
    <td style="padding:15px">Creates a link override.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_types/{pathv1}/link_type_properties/{pathv2}/link_overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId(linkTypeId, linkTypePropertyId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a link override.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_types/{pathv1}/link_type_properties/{pathv2}/link_overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesLinkTypePropertyIdLinkOverridesId(id, linkTypeId, linkTypePropertyId, body, callback)</td>
    <td style="padding:15px">Updates an existing link override.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_types/{pathv1}/link_type_properties/{pathv2}/link_overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkOverridesDelete(id, linkTypeId, linkTypePropertyId, callback)</td>
    <td style="padding:15px">Deletes a link override.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_types/{pathv1}/link_type_properties/{pathv2}/link_overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkPropertyValuesGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the link property values collection with no relation to a specific link.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_property_values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkPropertyValuesPut(body, callback)</td>
    <td style="padding:15px">Updates an existing link property value or multiple values. Creates if does not exist.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_property_values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkPropertyValuesGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the link property value.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_property_values/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1LinksLinkIdLinkPropertyValues(linkId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the link property values collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/links/{pathv1}/link_property_values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1LinksLinkIdLinkPropertyValues(linkId, body, callback)</td>
    <td style="padding:15px">Updates an existing link property value or created if does not exist.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/links/{pathv1}/link_property_values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1LinksLinkIdLinkPropertyValuesId(linkId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the link property value.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/links/{pathv1}/link_property_values/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1LinksLinkIdLinkPropertyValuesId(id, linkId, body, callback)</td>
    <td style="padding:15px">Updates an existing link property value.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/links/{pathv1}/link_property_values/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkPropertyValuesInitAbsentValues(linkId, callback)</td>
    <td style="padding:15px">Creates absent link property values.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/links/{pathv1}/link_property_values/init_absent_values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkTypePropertiesGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the link type properties collection with no relation to a specific link type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_type_properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkTypePropertiesPut(body, callback)</td>
    <td style="padding:15px">Updates an existing link type property or multiple properties.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_type_properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkTypePropertiesPost(body, callback)</td>
    <td style="padding:15px">Creates an new link type property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_type_properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkTypePropertiesGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a link type property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_type_properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1LinkTypesLinkTypeIdLinkTypeProperties(linkTypeId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the link type properties collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_types/{pathv1}/link_type_properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1LinkTypesLinkTypeIdLinkTypeProperties(linkTypeId, body, callback)</td>
    <td style="padding:15px">Updates an existing link type property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_types/{pathv1}/link_type_properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWapiV1LinkTypesLinkTypeIdLinkTypeProperties(linkTypeId, body, callback)</td>
    <td style="padding:15px">Creates an new link type property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_types/{pathv1}/link_type_properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId(linkTypeId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a link type property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_types/{pathv1}/link_type_properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1LinkTypesLinkTypeIdLinkTypePropertiesId(id, linkTypeId, body, callback)</td>
    <td style="padding:15px">Updates an existing link type property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_types/{pathv1}/link_type_properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkTypePropertiesDelete(id, linkTypeId, callback)</td>
    <td style="padding:15px">Deletes a link type property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_types/{pathv1}/link_type_properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkTypesGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the link types collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkTypesPut(body, callback)</td>
    <td style="padding:15px">Updates an existing link type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkTypesPost(body, callback)</td>
    <td style="padding:15px">Creates an new link type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkTypesGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a link type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1LinkTypesId(id, body, callback)</td>
    <td style="padding:15px">Updates an existing link type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulLinkTypesDelete(id, callback)</td>
    <td style="padding:15px">Deletes a link type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/link_types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVisLinksGet(linkId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the vis links collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/links/{pathv1}/vis_links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVisLinksPut(visLink, linkId, callback)</td>
    <td style="padding:15px">Updates an existing vis link.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/links/{pathv1}/vis_links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVisLinksGetById(linkId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the vis link.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/links/{pathv1}/vis_links/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeCategoriesGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the node categories collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeCategoriesPut(body, callback)</td>
    <td style="padding:15px">Updates an existing node category.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeCategoriesPost(body, callback)</td>
    <td style="padding:15px">Creates a node category.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeCategoriesGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a node category.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_categories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1NodeCategoriesId(id, body, callback)</td>
    <td style="padding:15px">Updates an existing node category.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_categories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeCategoriesDelete(id, callback)</td>
    <td style="padding:15px">Deletes a node category.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_categories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeOverridesGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the general node overrides collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeOverridesPut(body, callback)</td>
    <td style="padding:15px">Updates an existing node override.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeOverridesPost(body, callback)</td>
    <td style="padding:15px">Creates a node override.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeOverridesGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a node override.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_overrides/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverrides(nodeTypePropertyId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the node overrides collection for a property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_type_properties/{pathv1}/node_overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1NodeTypePropertiesNodeTypePropertyIdNodeOverridesId(nodeTypePropertyId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a node override.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_type_properties/{pathv1}/node_overrides/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides(nodeTypeId, nodeTypePropertyId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the node overrides collection for a property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_types/{pathv1}/node_type_properties/{pathv2}/node_overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverrides(nodeTypeId, nodeTypePropertyId, body, callback)</td>
    <td style="padding:15px">Creates a node override.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_types/{pathv1}/node_type_properties/{pathv2}/node_overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId(nodeTypeId, nodeTypePropertyId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a node override.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_types/{pathv1}/node_type_properties/{pathv2}/node_overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesNodeTypePropertyIdNodeOverridesId(id, nodeTypeId, nodeTypePropertyId, body, callback)</td>
    <td style="padding:15px">Updates an existing node override.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_types/{pathv1}/node_type_properties/{pathv2}/node_overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeOverridesDelete(id, nodeTypeId, nodeTypePropertyId, callback)</td>
    <td style="padding:15px">Deletes a node override.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_types/{pathv1}/node_type_properties/{pathv2}/node_overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodePropertyValuesGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the node property values collection with no relation to a specific node.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_property_values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodePropertyValuesPut(body, callback)</td>
    <td style="padding:15px">Updates an existing node property value or multiple values. Creates if does not exist.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_property_values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodePropertyValuesGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the node property value.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_property_values/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1NodesNodeIdNodePropertyValues(nodeId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the node property values collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}/node_property_values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1NodesNodeIdNodePropertyValues(nodeId, body, callback)</td>
    <td style="padding:15px">Updates an existing node property value or created if does not exist.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}/node_property_values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1NodesNodeIdNodePropertyValuesId(nodeId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the node property value.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}/node_property_values/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1NodesNodeIdNodePropertyValuesId(id, nodeId, body, callback)</td>
    <td style="padding:15px">Updates an existing node property value.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}/node_property_values/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodePropertyValuesInitAbsentValues(nodeId, callback)</td>
    <td style="padding:15px">Creates absent node property values.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}/node_property_values/init_absent_values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeTypePropertiesGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the node type properties collection with no relation to a specific node type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_type_properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeTypePropertiesPut(body, callback)</td>
    <td style="padding:15px">Updates an existing node type property or multiple properties.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_type_properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeTypePropertiesPost(body, callback)</td>
    <td style="padding:15px">Creates an new node type property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_type_properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeTypePropertiesGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a node type property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_type_properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1NodeTypesNodeTypeIdNodeTypeProperties(nodeTypeId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the node type properties collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_types/{pathv1}/node_type_properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1NodeTypesNodeTypeIdNodeTypeProperties(nodeTypeId, body, callback)</td>
    <td style="padding:15px">Updates an existing node type property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_types/{pathv1}/node_type_properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWapiV1NodeTypesNodeTypeIdNodeTypeProperties(nodeTypeId, body, callback)</td>
    <td style="padding:15px">Creates an new node type property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_types/{pathv1}/node_type_properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId(nodeTypeId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a node type property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_types/{pathv1}/node_type_properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1NodeTypesNodeTypeIdNodeTypePropertiesId(id, nodeTypeId, body, callback)</td>
    <td style="padding:15px">Updates an existing node type property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_types/{pathv1}/node_type_properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeTypePropertiesDelete(id, nodeTypeId, callback)</td>
    <td style="padding:15px">Deletes a node type property.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_types/{pathv1}/node_type_properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeTypesGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the node types collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeTypesPut(body, callback)</td>
    <td style="padding:15px">Updates an existing node type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeTypesPost(body, callback)</td>
    <td style="padding:15px">Creates an new node type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeTypesGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a node type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1NodeTypesId(id, body, callback)</td>
    <td style="padding:15px">Updates an existing node type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeTypesDelete(id, callback)</td>
    <td style="padding:15px">Deletes a node type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/node_types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodesGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the nodes collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodesPut(body, callback)</td>
    <td style="padding:15px">Updates an existing node.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodesPost(body, callback)</td>
    <td style="padding:15px">Creates a node.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodesGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the node.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1NodesId(id, body, callback)</td>
    <td style="padding:15px">Updates an existing node.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodesDelete(id, callback)</td>
    <td style="padding:15px">Deletes a node.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodesGetAliases(masterNodeId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the collection of node aliases.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}/aliases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodesPostAlias(masterNodeId, body, callback)</td>
    <td style="padding:15px">Creates a node alias.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}/aliases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1NodesSeparateLinks(visLinksSepartionData, callback)</td>
    <td style="padding:15px">Visually separates all links between given nodes.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/separate_links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulDiagramSettingsGetDiagramSettings(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the node diagram settings.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}/diagram_settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulDiagramSettingsPut(id, body, callback)</td>
    <td style="padding:15px">Updates diagram settings of an existing node.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}/diagram_settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeMapCoordinatesGetMapCoordinates(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the node map coordinates.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}/map_coordinates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulNodeMapCoordinatesPut(id, body, callback)</td>
    <td style="padding:15px">Updates map coordinates of an existing node.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}/map_coordinates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVisNodesGet(nodeId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the vis nodes collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}/vis_nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVisNodesPut(visNode, nodeId, callback)</td>
    <td style="padding:15px">Updates an existing vis node.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}/vis_nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVisNodesGetById(nodeId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the vis node.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}/vis_nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVisNodesGetOriginal(nodeId, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the original (non-reference) Vis Node of the specified node.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}/vis_nodes/original?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVisNodesGetReferences(nodeId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the collection of reference vis nodes of the specified node.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/nodes/{pathv1}/vis_nodes/references?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRackContainerDesignsGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the container designs collection with no relation to a specific a rack type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/rack_container_designs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRackContainerDesignsPut(body, callback)</td>
    <td style="padding:15px">Update a rack type's container design.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/rack_container_designs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRackContainerDesignsPost(body, callback)</td>
    <td style="padding:15px">Creates an new rack type's container design.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/rack_container_designs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRackContainerDesignsGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the container design belonging to the rack type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/rack_container_designs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1RackTypesRackTypeIdRackContainerDesigns(rackTypeId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the container designs collection for a rack type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/rack_types/{pathv1}/rack_container_designs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWapiV1RackTypesRackTypeIdRackContainerDesigns(rackTypeId, body, callback)</td>
    <td style="padding:15px">Creates an new rack type's container design.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/rack_types/{pathv1}/rack_container_designs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1RackTypesRackTypeIdRackContainerDesignsId(rackTypeId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the container design belonging to the rack type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/rack_types/{pathv1}/rack_container_designs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1RackTypesRackTypeIdRackContainerDesignsId(id, rackTypeId, body, callback)</td>
    <td style="padding:15px">Update a rack type's container design.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/rack_types/{pathv1}/rack_container_designs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRackContainerDesignsDelete(id, rackTypeId, callback)</td>
    <td style="padding:15px">Deletes the rack type's container design.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/rack_types/{pathv1}/rack_container_designs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRackTypesGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the rack types collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/rack_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRackTypesPut(body, callback)</td>
    <td style="padding:15px">Updates an existing rack type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/rack_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRackTypesPost(body, callback)</td>
    <td style="padding:15px">Creates an new rack type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/rack_types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRackTypesGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the rack type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/rack_types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1RackTypesId(id, body, callback)</td>
    <td style="padding:15px">Updates an existing rack type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/rack_types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRackTypesDelete(id, callback)</td>
    <td style="padding:15px">Deletes a rack type.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/rack_types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRacksGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the racks collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/racks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRacksPost(body, callback)</td>
    <td style="padding:15px">Creates a rack.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/racks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRacksGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the rack.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/racks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRacksPut(id, body, callback)</td>
    <td style="padding:15px">Updates an existing rack.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/racks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRacksDelete(id, callback)</td>
    <td style="padding:15px">Deletes a rack.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/racks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRackContainersGet(rackId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the rack containers collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/racks/{pathv1}/containers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRackContainersGetById(rackId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the rack container.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/racks/{pathv1}/containers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRolesGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the user roles collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulRolesGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a user role.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulSearchGet(q, parentDiagram, includeSubdiagrams, exactMatch, resourceType, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Search resources.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulWorkOrderTasksGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the work order task's collection with no relation to a specific work order.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWapiV1WorkOrdersWorkOrderIdTasks(workOrderId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the work order task's collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/work_orders/{pathv1}/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulWorkOrderTasksPut(workOrderTask, workOrderId, callback)</td>
    <td style="padding:15px">Updates an existing work order task.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/work_orders/{pathv1}/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulWorkOrderTasksPost(workOrderTask, workOrderId, callback)</td>
    <td style="padding:15px">Creates a work order task.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/work_orders/{pathv1}/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulWorkOrderTasksGetById(workOrderId, id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a work order task.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/work_orders/{pathv1}/tasks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulWorkOrderTasksDelete(id, workOrderId, callback)</td>
    <td style="padding:15px">Deletes a work order task.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/work_orders/{pathv1}/tasks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulUsersGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the netTerrain users collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulUsersPut(body, callback)</td>
    <td style="padding:15px">Updates an existing user.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulUsersPost(body, callback)</td>
    <td style="padding:15px">Creates a user.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulUsersGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a netTerrain user.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1UsersId(id, body, callback)</td>
    <td style="padding:15px">Updates an existing user.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulUsersDelete(id, callback)</td>
    <td style="padding:15px">Deletes a user.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulUsersSetPassword(id, password, callback)</td>
    <td style="padding:15px">Sets a password for a user.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/users/{pathv1}/set_password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVendorsGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the vendors collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/vendors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVendorsPut(vendor, callback)</td>
    <td style="padding:15px">Updates an existing vendor.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/vendors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVendorsPost(vendor, callback)</td>
    <td style="padding:15px">Creates a vendor.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/vendors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVendorsGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a vendor.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/vendors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWapiV1VendorsId(id, vendor, callback)</td>
    <td style="padding:15px">Updates an existing vendor.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/vendors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVendorsDelete(id, callback)</td>
    <td style="padding:15px">Deletes a vendor.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/vendors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVisLinkDisplayedFieldsGet(visLinkId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the vis link displayed fields collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/vis_links/{pathv1}/displayed_fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVisLinkDisplayedFieldsPut(linkDisplayedField, visLinkId, callback)</td>
    <td style="padding:15px">Updates an existing displayed field of a vis link property value.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/vis_links/{pathv1}/displayed_fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVisLinkDisplayedFieldsPost(displayedField, visLinkId, callback)</td>
    <td style="padding:15px">Creates a displayed field for a link's property value.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/vis_links/{pathv1}/displayed_fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVisLinkDisplayedFieldsGetById(linkPropertyValueId, visLinkId, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the vis link displayed field value.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/vis_links/{pathv1}/displayed_fields/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVisNodeDisplayedFieldsGet(visNodeId, page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the vis node displayed fields collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/vis_nodes/{pathv1}/displayed_fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVisNodeDisplayedFieldsPut(nodeDisplayedField, visNodeId, callback)</td>
    <td style="padding:15px">Updates an existing displayed field of a vis node property value.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/vis_nodes/{pathv1}/displayed_fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVisNodeDisplayedFieldsPost(displayedField, visNodeId, callback)</td>
    <td style="padding:15px">Creates a displayed field for a node's property value.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/vis_nodes/{pathv1}/displayed_fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulVisNodeDisplayedFieldsGetById(nodePropertyValueId, visNodeId, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the vis node displayed field value.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/vis_nodes/{pathv1}/displayed_fields/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulWorkOrdersGet(page, perPage, offset, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets the work order's collection.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/work_orders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulWorkOrdersPut(workOrder, callback)</td>
    <td style="padding:15px">Updates an existing work order.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/work_orders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulWorkOrdersPost(workOrder, callback)</td>
    <td style="padding:15px">Creates a work order.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/work_orders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulWorkOrdersGetById(id, expand, showChildCollections, callback)</td>
    <td style="padding:15px">Gets a work order.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/work_orders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rESTfulWorkOrdersDelete(id, callback)</td>
    <td style="padding:15px">Deletes a work order.</td>
    <td style="padding:15px">{base_path}/{version}/wapi/v1/work_orders/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
